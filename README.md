# jvm-rust

#### 介绍

参考《自己动手写Java虚拟机》使用Rust实现Java虚拟机。除参考本书所完成功能外，还实现了对线程、lambda表达式（lambda内部不支持使用::(双冒号)运算符、外部对象）的初步支持。

#### 使用说明

1. 直接运行：cargo run -- -Xjre 本地jre路径 -cp  测试class文件所在目录 文件名称

   ```
   cargo run -- -Xjre /home/lixun/jdk1.8.0_381/jre -cp ./java base/HelloWorld
   ```

   ```
   cargo run -- -Xjre /home/lixun/jdk1.8.0_381/jre -cp ./java base/PriorityQueueTest
   ```

   ```
   cargo run -- -Xjre /home/lixun/jdk1.8.0_381/jre -cp ./java thread/ThreadTest
   ```

   

2. 运行测试方法：修改src/test/mod.rs的 X_JRE_OPTHIN 为本地jre路径。-cp为测试class文件所在目录

   ```
   cargo test helloworld
   ```

   ```
   cargo test string_build_test
   ```

   ```
   cargo test synchronized_test
   ```









