use std::{fmt::Debug, rc::Rc, cell::RefCell};

use crate::rtda::Frame;

use super::BytecodeReader;

#[allow(unused_variables)]
pub trait Instruction: Debug {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        // nothing to do
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        // nothing to do
    }
}