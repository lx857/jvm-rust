use std::{rc::Rc, cell::RefCell};

use crate::{rtda::Frame, instructions::base::{Instruction, BytecodeReader}};


// fload指令的索引来自操作数，其他来自指令
// Load float from local variable
#[derive(Debug, Default)]
pub struct FLOAD {
    index: usize
}

impl Instruction for FLOAD {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.index = reader.read_u8() as usize;
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _fload(frame, self.index);
    }
}

impl FLOAD {
    pub fn set_index(&mut self, index: usize) {
        self.index = index;
    } 
}

#[derive(Debug, Default)]
pub struct FLOAD_0 { }

impl Instruction for FLOAD_0 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _fload(frame, 0);
    }
}

#[derive(Debug, Default)]
pub struct FLOAD_1 { }

impl Instruction for FLOAD_1 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _fload(frame, 1);
    }
}

#[derive(Debug, Default)]
pub struct FLOAD_2 { }

impl Instruction for FLOAD_2 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _fload(frame, 2);
    }
}

#[derive(Debug, Default)]
pub struct FLOAD_3 { }

impl Instruction for FLOAD_3 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _fload(frame, 3);
    }
}

// 通用加载，从局部变量表获取变量，推入操作数栈顶
fn _fload(frame: Rc<RefCell<Frame>>, index: usize) {
    let val = frame.borrow().get_local_vars().borrow().get_float(index);
    frame.borrow().get_operand_stack().borrow_mut().push_float(val);
}