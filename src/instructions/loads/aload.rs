use std::{rc::Rc, cell::RefCell};

use crate::{rtda::Frame, instructions::base::{Instruction, BytecodeReader}};


// aload指令的索引来自操作数，其他来自指令
// Load reference from local variable
#[derive(Debug, Default)]
pub struct ALOAD {
    index: usize
}

impl Instruction for ALOAD {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.index = reader.read_u8() as usize;
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _aload(frame, self.index);
    }
}

impl ALOAD {
    pub fn set_index(&mut self, index: usize) {
        self.index = index;
    } 
}

#[derive(Debug, Default)]
pub struct ALOAD_0 { }

impl Instruction for ALOAD_0 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _aload(frame, 0);
    }
}

#[derive(Debug, Default)]
pub struct ALOAD_1 { }

impl Instruction for ALOAD_1 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _aload(frame, 1);
    }
}

#[derive(Debug, Default)]
pub struct ALOAD_2 { }

impl Instruction for ALOAD_2 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _aload(frame, 2);
    }
}

#[derive(Debug, Default)]
pub struct ALOAD_3 { }

impl Instruction for ALOAD_3 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _aload(frame, 3);
    }
}

// 通用加载，从局部变量表获取变量，推入操作数栈顶
fn _aload(frame: Rc<RefCell<Frame>>, index: usize) {
    let val = frame.borrow().get_local_vars().borrow().get_ref(index);
    frame.borrow().get_operand_stack().borrow_mut().push_ref(val);
}