use std::{rc::Rc, cell::RefCell};

use crate::{rtda::Frame, instructions::base::{Instruction, BytecodeReader}};


// lload指令的索引来自操作数，其他来自指令
// Load long from local variable
#[derive(Debug, Default)]
pub struct LLOAD {
    index: usize
}

impl Instruction for LLOAD {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.index = reader.read_u8() as usize;
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _lload(frame, self.index);
    }
}

impl LLOAD {
    pub fn set_index(&mut self, index: usize) {
        self.index = index;
    } 
}

#[derive(Debug, Default)]
pub struct LLOAD_0 { }

impl Instruction for LLOAD_0 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _lload(frame, 0);
    }
}

#[derive(Debug, Default)]
pub struct LLOAD_1 { }

impl Instruction for LLOAD_1 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _lload(frame, 1);
    }
}

#[derive(Debug, Default)]
pub struct LLOAD_2 { }

impl Instruction for LLOAD_2 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _lload(frame, 2);
    }
}

#[derive(Debug, Default)]
pub struct LLOAD_3 { }

impl Instruction for LLOAD_3 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _lload(frame, 3);
    }
}

// 通用加载，从局部变量表获取变量，推入操作数栈顶
fn _lload(frame: Rc<RefCell<Frame>>, index: usize) {
    let val = frame.borrow().get_local_vars().borrow().get_long(index);
    frame.borrow().get_operand_stack().borrow_mut().push_long(val);
}