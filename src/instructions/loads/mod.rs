// 加载指令从局部变量表获取变量，然后推入操作数栈顶。
mod aload;  // 操作引用类型变量
mod dload;  // 操作double类型变量
mod fload;  // 操作float变量
mod iload;  // 操作int变量
mod lload;  // 操作long变量
mod xaload; // 操作数组

pub use aload::{ALOAD, ALOAD_0, ALOAD_1, ALOAD_2, ALOAD_3};
pub use dload::{DLOAD, DLOAD_0, DLOAD_1, DLOAD_2, DLOAD_3};
pub use fload::{FLOAD, FLOAD_0, FLOAD_1, FLOAD_2, FLOAD_3};
pub use iload::{ILOAD, ILOAD_0, ILOAD_1, ILOAD_2, ILOAD_3};
pub use lload::{LLOAD, LLOAD_0, LLOAD_1, LLOAD_2, LLOAD_3};
pub use xaload::*;