use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};

// 交换栈顶的两个变量
// Swap the top two operand stack values
#[derive(Debug, Default)]
pub struct SWAP {}

// bottom -> top
// [...][c][b][a]
//           \/
//           /\
//          V  V
// [...][c][a][b]
impl Instruction for SWAP {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let mut stack = stack.borrow_mut();
        let slot1 = stack.pop_slot();
        let slot2 = stack.pop_slot();
        stack.push_slot(slot1);
        stack.push_slot(slot2);
    }
}
