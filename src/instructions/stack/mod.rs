// 直接对操作数栈进行操作
mod pop;  // 将栈顶变量弹出
mod dup;  // 复制栈顶变量
mod swap; // 交换栈顶的两个变量

pub use pop::*;
pub use dup::*;
pub use swap::*;