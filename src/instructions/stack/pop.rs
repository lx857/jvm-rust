use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};

// 将栈顶变量弹出
// Pop the top operand stack value
#[derive(Debug, Default)]
pub struct POP { }

// bottom -> top
// [...][c][b][a]
//             |
//             V
// [...][c][b]
impl Instruction for POP {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let _ = stack.borrow_mut().pop_slot();
    }
}

// Pop the top one or two operand stack values
#[derive(Debug, Default)]
pub struct POP2 { }

// bottom -> top
// [...][c][b][a]
//          |  |
//          V  V
// [...][c]
impl Instruction for POP2 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let _ = stack.borrow_mut().pop_slot();
        let _ = stack.borrow_mut().pop_slot();
    }
}

