use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::{Instruction, BytecodeReader, self}, rtda::{heap, Frame}};


#[derive(Debug, Default)]
pub struct INVOKE_INTERFACE {
    index: usize, //被调用的方法的符号引用（CONSTANT_Methodref_info或者CONSTANT_InterfaceMethodref_info常量）
    // count: u8,
    // zero: u8,
}

impl Instruction for INVOKE_INTERFACE {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.index = reader.read_u16() as usize;
        reader.read_u8(); // count
        reader.read_u8(); // must bo 0
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let method = frame.borrow().get_method();
        let method_ref = method;
        let current_class = method_ref.get_class();
        let cp = current_class.get_constant_pool().unwrap();
        let method_ref = cp.get_constant(self.index).unwrap().interface_method_ref();
        let mut mr_mut = method_ref.lock().unwrap();
        let resolved_method = mr_mut.resolved_interface_method();
        // 静态或私有方法抛出异常
        if resolved_method.is_static() || resolved_method.is_private() {
            panic!("java.lang.IncompatibleClassChangeError");
        }
        // 获取this引用   stack: n n this x x x x; x为参数代表4个。 实例方法会在第五个位置加上this引用
        let _ref = frame.borrow().get_operand_stack().borrow().get_ref_from_top(resolved_method.get_arg_slot_count() - 1);
        if _ref.is_none() {
            panic!("java.lang.NullPointerException");
        }
        let _ref = _ref.unwrap();
        // 引用对象的类没有实现解析出来的接口抛出异常
        let ref_class = unsafe { &*_ref }.get_class();
        if !ref_class.is_implements(&mr_mut.resolved_class()) {
            panic!("java.lang.IncompatibleClassChangeError")
        }
        let method_to_be_invoked = heap::lookup_method_in_class(ref_class, mr_mut.get_name(), mr_mut.get_descriptor());
        // 未查找到或方法是抽象的抛出异常
        if method_to_be_invoked.is_none() || method_to_be_invoked.as_ref().unwrap().is_abstract() {
            panic!("java.lang.AbstractMethodError");
        }
        // 非public抛出异常
        if !method_to_be_invoked.as_ref().unwrap().is_public() {
            panic!("java.lang.IllegalAccessError");
        }

        base::invoke_method(frame, method_to_be_invoked.unwrap());
    }


}
