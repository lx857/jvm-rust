use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::{Instruction, BytecodeReader, self}, rtda::{heap::{self, string_pool}, OperandStack, Frame}};


#[derive(Debug, Default)]
pub struct INVOKE_VIRTUAL {
    index: usize, //被调用的方法的符号引用（CONSTANT_Methodref_info或者CONSTANT_InterfaceMethodref_info常量）
}

impl Instruction for INVOKE_VIRTUAL {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.index = reader.read_u16() as usize;
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let method = frame.borrow().get_method();
        let current_class = method.get_class();
        let cp = current_class.get_constant_pool().unwrap();
        let method_ref = cp.get_constant(self.index).unwrap().method_ref();
        let resolved_method = method_ref.lock().unwrap().resolved_method();
        // 静态方法，抛出异常
        if resolved_method.is_static() {
            panic!("java.lang.IncompatibleClassChangeError")
        }
        // 获取this引用   stack: n n this x x x x; x为参数代表4个。 实例方法会在第五个位置加上this引用
        let _ref = frame.borrow().get_operand_stack().borrow().get_ref_from_top(resolved_method.get_arg_slot_count() - 1);
        if _ref.is_none() {
            panic!("java.lang.NullPointerException");
        }
        let _ref = _ref.unwrap();
        let arc_class = unsafe { &*_ref }.get_class();
        // let ref_class = unsafe { &*arc_class };
        // 确保protected方法只能被声明该方法的类或子类调用
        if resolved_method.is_protected()
        && resolved_method.get_class().is_sub_class_of(current_class.as_ref())
        && resolved_method.get_class().get_package_name() != current_class.get_package_name()
        && !std::ptr::eq(arc_class.as_ref(), current_class.as_ref())
        && !arc_class.is_sub_class_of(current_class.as_ref()) {
            panic!("java.lang.IllegalAccessError")
        }
        // 从方法对象类中解析出需要调用的方法
        let method_to_be_invoked = heap::lookup_method_in_class(arc_class.clone(), &resolved_method.get_name(), &resolved_method.get_descriptor());
        // 未查找到或方法是抽象的抛出异常
        if method_to_be_invoked.is_none() || method_to_be_invoked.as_ref().unwrap().is_abstract() {
            panic!("java.lang.AbstractMethodError");
        }

        base::invoke_method(frame, method_to_be_invoked.unwrap());
    

    }
}


fn _println(stack: Rc<RefCell<OperandStack>>, descriptor: &str) {
    match descriptor {
        "(Z)V" => println!("{}", stack.borrow_mut().pop_int() != 0),
        "(C)V" => println!("{}", stack.borrow_mut().pop_int()),
        "(B)V" => println!("{}", stack.borrow_mut().pop_int()),
        "(S)V" => println!("{}", stack.borrow_mut().pop_int()),
        "(I)V" => println!("{}", stack.borrow_mut().pop_int()),
        "(J)V" => println!("{}", stack.borrow_mut().pop_long()),
        "(F)V" => println!("{}", stack.borrow_mut().pop_float()),
        "(D)V" => println!("{}", stack.borrow_mut().pop_double()),
        "(Ljava/lang/String;)V" => {
            let j_str = stack.borrow_mut().pop_ref();
            let rs_str = string_pool::rs_string(j_str.unwrap());
            println!("{}", rs_str);
        },
        _ => panic!("println: {}", descriptor)
    }
    stack.borrow_mut().pop_ref();
}
