use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};


#[derive(Debug, Default)]
pub struct ARRAY_LENGTH {}

impl Instruction for ARRAY_LENGTH {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let arr_ref = stack.borrow_mut().pop_ref();
        if arr_ref.is_none() {
            panic!("java.lang.NullPointerException");
        }

        let arr_len = unsafe { &*arr_ref.unwrap() }.array_length();
        
        stack.borrow_mut().push_int(arr_len as i32);
    }
}