mod new;             // 创建类实例
mod putstatic;       // 给类的某个静态变量赋值
mod getstatic;       // 从静态变量中取出相应的值，然后推入操作数栈顶。
mod putfield;        // 给实例变量赋值
mod getfield;        // 获取对象的实例变量值，然后推入操作数栈
mod instanceof;      // 判断对象是否是某个类的实例（或者对象的类是否实现了某个接口）
mod checkcast;       // 与instanceof很像,判断失败直接抛出异常

// 方法调用
// 首先，方法调用指令需要n+1个操作数，其中第1个操作数是uint16索引，在字节码中紧跟在指令操作码的后面。
// 通过这个索引，可以从当前类的运行时常量池中找到一个方法符号引用，解析这个符号引用就可以得到一个方法。
// 注意，这个方法并不一定就是最终要调用的那个方法，所以可能还需要一个查找过程才能找到最终要调用的方法。
// 剩下的n个操作数是要传递给被调用方法的参数，从操作数栈中弹出。
mod invokevirtual;   // 其他,需要动态绑定的实例方法
mod invokespecial;   // 调用无须动态绑定的实例方法，包括构造函数、私有方法和通过super关键字调用的超类方法。
mod invokestatic;    // 调用静态方法
mod invokeinterface; // 针对接口类型的引用调用方法,需要动态绑定的实例方法

mod newarray;        // 创建基本类型数组
mod anewarray;       // 创建引用类型数组。
mod arraylength;     // 获取数组长度
mod multianewarray;  // 创建多维数组
mod athrow;          // 抛出异常
mod monitor;         // 
mod invokedynamic;   // 

pub use new::NEW;
pub use putstatic::PUT_STATIC;
pub use getstatic::GET_STATIC;
pub use putfield::PUT_FIELD;
pub use getfield::GET_FIELD;
pub use instanceof::INSTANCE_OF;
pub use checkcast::CHECK_CAST;
pub use invokevirtual::INVOKE_VIRTUAL;
pub use invokespecial::INVOKE_SPECIAL;
pub use invokestatic::INVOKE_STATIC;
pub use invokeinterface::INVOKE_INTERFACE;
pub use newarray::*;
pub use anewarray::ANEW_ARRAY;
pub use arraylength::ARRAY_LENGTH;
pub use multianewarray::MULTI_ANEW_ARRAY;
pub use athrow::ATHROW;
pub use monitor::*;
pub use invokedynamic::INVOKE_DYNAMIC;