use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::{Instruction, BytecodeReader, self}, rtda::Frame};

#[derive(Debug, Default)]
pub struct GET_STATIC {
    index: usize
}

impl Instruction for GET_STATIC {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.index = reader.read_u16() as usize;
    }

    // 从静态变量中取出相应的值，然后推入操作数栈顶。
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let cp = frame.borrow().get_method().get_class().get_constant_pool().unwrap();
        let field_ref = cp.get_constant(self.index).unwrap().field_ref();
        // if let Constant::FieldRef(class_ref) = constant {
        let field = field_ref.lock().unwrap().resolved_field();
        let class = field.get_class();
        // init class
        if !class.init_started() {
            // 获取到此锁可进行初始化。
            let init_lock = class.get_init_lock();
            let _lock = init_lock.lock().unwrap();
            // TODO 231028 不清楚为什么不直接往下执行，而是在执行一次此指令，后续测试。
            // 当前帧的nextPC字段已经指向下一条指令，所以需要修改nextPC，让它重新指向当前指令。
            frame.borrow_mut().revert_next_pc();
            // 再次判断，若在其他线程初始化后获取到锁则跳过初始化
            if !class.init_started() {
                base::init_class(frame.borrow().get_thread(), class);
            }
            return;
        }

        // 非静态字段，抛出异常
        if !field.is_static() {
            panic!("java.lang.IncompatibleClassChangeError")
        }
        
        let descriptor = field.get_descriptor();
        let slot_id = field.get_slot_id();
        let slots = class.get_static_vars();
        let stack = frame.borrow().get_operand_stack();
        // 从操作数栈中弹出相应的值，然后赋给静态变量。
        match &descriptor[..1] {
            "Z" | "B"  | "C" | "S" | "I" => stack.borrow_mut().push_int(slots.read().unwrap().get_int(slot_id)),
            "F" => stack.borrow_mut().push_float(slots.read().unwrap().get_float(slot_id)),
            "J" => stack.borrow_mut().push_long(slots.read().unwrap().get_long(slot_id)),
            "D" => stack.borrow_mut().push_double(slots.read().unwrap().get_double(slot_id)),
            "L" | "[" => stack.borrow_mut().push_ref(slots.read().unwrap().get_ref(slot_id)),
            _ => ()
        }
        // }
    }


}
