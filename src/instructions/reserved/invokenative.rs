use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame, native};

// 调用本地方法 使用预留的0xfe指令实现
#[derive(Debug, Default)]
pub struct INVOKE_NATIVE {}

impl Instruction for INVOKE_NATIVE {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let method = frame.borrow().get_method();
        let class_name = method.get_class().get_name();
        let method_name = method.get_name();
        let method_descriptor = method.get_descriptor();
        let native_method = native::find_native_method(&class_name, &method_name, &method_descriptor);
        if let Some(native_method) = native_method {
            // let loader = method.get_class().get_class_loader().unwrap();
            // // let lock = loader.lock().unwrap();
            // if loader.try_lock().is_err() {
            //     println!("classloader此时有占用！")
            // }
            native_method(frame);
        } else {
            let method_info = class_name + "." + &method_name + &method_descriptor;
            panic!("java.lang.UnsatisfiedLinkError: {}", method_info);
        }
    }
}