use crate::instructions::base::Instruction;

// 什么也不做
#[derive(Debug, Default)]
pub struct NOP { }

impl Instruction for NOP { }