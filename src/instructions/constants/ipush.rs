use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::{Instruction, BytecodeReader}, rtda::Frame};


// 从操作数中获取一个byte型整数，扩展成int型，然后推入栈顶。
// 估计是紧跟着这个指令后面的几个byte
#[derive(Debug, Default)]
pub struct BIPUSH {
    val: i8
} 

impl Instruction for BIPUSH {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.val = reader.read_i8();
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let i = self.val as i32;
        frame.borrow().get_operand_stack().borrow_mut().push_int(i);
    }
}

// 操作数中获取一个short型整数，扩展成int型，然后推入栈顶。
#[derive(Debug, Default)]
pub struct SIPUSH {
    val: i16
}

impl Instruction for SIPUSH {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.val = reader.read_i16();
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let i = self.val as i32;
        frame.borrow().get_operand_stack().borrow_mut().push_int(i);
    }
}