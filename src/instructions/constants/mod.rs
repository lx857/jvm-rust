// 常量指令把常量推入操作数栈顶。常量可以来自三个地方：隐含在操作码里、操作数和运行时常量池。
mod nop;      // 什么也不做
mod constant; // 把隐含在操作码中的常量值推入操作数栈顶。
mod ipush;    // 获取紧跟着这个指令后面的几个byte转换为int数据推入栈顶
mod ldc;      // 从运行时常量池中加载常量值，并把它推入操作数栈顶。

pub use nop::*;
pub use constant::*;
pub use ipush::*;
pub use ldc::*;