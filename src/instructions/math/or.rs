use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};


// Boolean OR int
#[derive(Debug, Default)]
pub struct IOR {}

impl Instruction for IOR {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let v2 = stack.borrow_mut().pop_int();
        let v1 = stack.borrow_mut().pop_int();
        let result = v1 | v2;
        stack.borrow_mut().push_int(result);
    }
}

// Boolean OR long
#[derive(Debug, Default)]
pub struct LOR {}

impl Instruction for LOR {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let v2 = stack.borrow_mut().pop_long();
        let v1 = stack.borrow_mut().pop_long();
        let result = v1 | v2;
        stack.borrow_mut().push_long(result);
    }
}
