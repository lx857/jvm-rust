use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};


// Boolean AND int
#[derive(Debug, Default)]
pub struct IAND {}

impl Instruction for IAND {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let v2 = stack.borrow_mut().pop_int();
        let v1 = stack.borrow_mut().pop_int();
        let result = v1 & v2;
        stack.borrow_mut().push_int(result);
    }
}

// Boolean AND long
#[derive(Debug, Default)]
pub struct LAND {}

impl Instruction for LAND {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let v2 = stack.borrow_mut().pop_long();
        let v1 = stack.borrow_mut().pop_long();
        let result = v1 & v2;
        stack.borrow_mut().push_long(result);
    }
}
