// TODO 231007 目前运算移除会直接panic，测试了下java默认溢出，后续全部修改 
// 《Java虚拟机规范》中并没有明确定义过整型数据溢出具体会得到什么计算结果，仅规定了在
// 处理整型数据时，只有除法指令（idiv和ldiv）以及求余指令（irem和lrem）中当出现除数为零时会导致
// 虚拟机抛出ArithmeticException异常，其余任何整型数运算场景都不应该抛出运行时异常。

mod add; // 加
mod sub; // 减
mod mul; // 乘
mod div; // 除
mod rem; // 取余
mod neg; // 取反
mod sh;  // 位运算-位移
mod and; // 位运算-位与(&)
mod or;  // 位运算-位或(|)
mod xor; // 位运算-异或(^)
mod iinc;// 局部变量表中的int变量增加常量值

pub use add::*;
pub use sub::*;
pub use mul::*;
pub use div::*;
pub use rem::*;
pub use neg::*;
pub use sh::*;
pub use and::*;
pub use or::*;
pub use xor::*;
pub use iinc::IINC;