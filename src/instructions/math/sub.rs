use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};


// Subtract double
#[derive(Debug, Default)]
pub struct DSUB {}

impl Instruction for DSUB {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let v2 = stack.borrow_mut().pop_double();
        let v1 = stack.borrow_mut().pop_double();
        let result = v1 - v2;
        stack.borrow_mut().push_double(result);
    }
}

// Subtract float
#[derive(Debug, Default)]
pub struct FSUB {}

impl Instruction for FSUB {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let v2 = stack.borrow_mut().pop_float();
        let v1 = stack.borrow_mut().pop_float();
        let result = v1 - v2;
        stack.borrow_mut().push_float(result);
    }
}


// Subtract int
#[derive(Debug, Default)]
pub struct ISUB {}

impl Instruction for ISUB {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let v2 = stack.borrow_mut().pop_int();
        let v1 = stack.borrow_mut().pop_int();
        let result = v1 - v2;
        stack.borrow_mut().push_int(result);
    }
}

// Subtract long
#[derive(Debug, Default)]
pub struct LSUB {}

impl Instruction for LSUB {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let v2 = stack.borrow_mut().pop_long();
        let v1 = stack.borrow_mut().pop_long();
        let result = v1 - v2;
        stack.borrow_mut().push_long(result);
    }
}