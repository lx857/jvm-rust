use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::{Instruction, BytecodeReader}, rtda::Frame};

// 给局部变量表中的int变量增加常量值，局部变量表索引和常量值都由指令的操作数提供。
// Increment local variable by constant
#[derive(Debug, Default)]
pub struct IINC {
    index: usize,
    count: i32
}

impl Instruction for IINC {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.index = reader.read_u8() as usize;
        self.count = reader.read_i8() as i32;
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let local_vars = frame.borrow().get_local_vars();
        let mut val = local_vars.borrow_mut().get_int(self.index);
        val += self.count;
        local_vars.borrow_mut().set_int(self.index, val);
    }
}

impl IINC {
    pub fn set_index(&mut self, index: usize) {
        self.index = index;
    } 

    pub fn set_count(&mut self, count: i32) {
        self.count = count;
    }
}