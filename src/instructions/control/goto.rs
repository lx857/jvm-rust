use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::{self, Instruction, BytecodeReader}, rtda::Frame};

// 进行无条件跳转

// Branch always
#[derive(Debug, Default)]
pub struct GOTO {
    offset: i32 // 可以为负数，测试出错了，classpy看了源码为0xfff3 应该是-13
}

impl Instruction for GOTO {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.offset = reader.read_i16() as i32;
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        base::branch(frame, self.offset)
    }
}
