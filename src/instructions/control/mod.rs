mod goto; // 进行无条件跳转
// switch-case语句有两种实现方式：如果case值可以编码成一个索引表，则实现成tableswitch指令；否则实现成lookupswitch指令。
mod tableswitch;
mod lookupswitch;
mod _return;

pub use goto::*;
pub use tableswitch::*;
pub use lookupswitch::*;
pub use _return::*;