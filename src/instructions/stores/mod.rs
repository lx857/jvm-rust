// 把变量从操作数栈顶弹出，然后存入局部变量表。
mod astore;  // 操作引用类型变量
mod dstore;  // 操作double类型变量
mod fstore;  // 操作float变量
mod istore;  // 操作int变量
mod lstore;  // 操作long变量
mod xastore; // 操作数组

pub use astore::{ASTORE, ASTORE_0, ASTORE_1, ASTORE_2, ASTORE_3};
pub use dstore::{DSTORE, DSTORE_0, DSTORE_1, DSTORE_2, DSTORE_3};
pub use fstore::{FSTORE, FSTORE_0, FSTORE_1, FSTORE_2, FSTORE_3};
pub use istore::{ISTORE, ISTORE_0, ISTORE_1, ISTORE_2, ISTORE_3};
pub use lstore::{LSTORE, LSTORE_0, LSTORE_1, LSTORE_2, LSTORE_3};
pub use xastore::*;