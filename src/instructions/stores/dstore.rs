use std::{rc::Rc, cell::RefCell};

use crate::{rtda::Frame, instructions::base::{BytecodeReader, Instruction}};

// dstore指令的索引来自操作数，其他来自指令
// Store int into local variable
#[derive(Debug, Default)]
pub struct DSTORE {
    index: usize
}

impl Instruction for DSTORE {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.index = reader.read_u8() as usize;
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _dstore(frame, self.index);
    }
}

impl DSTORE {
    pub fn set_index(&mut self, index: usize) {
        self.index = index;
    } 
}

#[derive(Debug, Default)]
pub struct DSTORE_0 { }

impl Instruction for DSTORE_0 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _dstore(frame, 0);
    }
}

#[derive(Debug, Default)]
pub struct DSTORE_1 { }

impl Instruction for DSTORE_1 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _dstore(frame, 1);
    }
}

#[derive(Debug, Default)]
pub struct DSTORE_2 { }

impl Instruction for DSTORE_2 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _dstore(frame, 2);
    }
}

#[derive(Debug, Default)]
pub struct DSTORE_3 { }

impl Instruction for DSTORE_3 {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _dstore(frame, 3);
    }
}


// 通用加载，把变量从操作数栈顶弹出，存入局部变量表。
fn _dstore(frame: Rc<RefCell<Frame>>, index: usize) {
    let val = frame.borrow().get_operand_stack().borrow_mut().pop_double();
    frame.borrow().get_local_vars().borrow_mut().set_double(index, val);
}