#![allow(non_camel_case_types)]

pub mod base;
pub mod factory;
mod comparisons; // 比较指令
mod constants;   // 常量指令
mod control;     // 控制指令
mod conversions; // 转换指令
mod extended;    // 扩展指令
mod loads;       // 加载指令
mod math;        // 数学指令
mod stack;       // 操作数栈指令
mod stores;      // 存储指令
mod references;  // 引用指令
mod reserved;    // 保留指令

pub use references::MONITOR_LOCKS;