// 加载类指令、存储类指令、ret指令和iinc指令需要按索引访问局部变量表，索引以uint8的形式存在字节码中。
// 对于大部分方法来说，局部变量表大小都不会超过256，所以用一字节来表示索引就够了。
// 但是如果有方法的局部变量表超过这限制呢？Java虚拟机规范定义了wide指令来扩展前述指令。
mod wide;
mod ifnull; // 根据栈顶引用是否是null进行跳转
mod goto_w;

pub use wide::*;
pub use ifnull::*;
pub use goto_w::*;