use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::{self, Instruction, BytecodeReader}, rtda::Frame};

// 与GOTO指令相似,进行无条件跳转
// Branch always (wide index)
#[derive(Debug, Default)]
pub struct GOTO_W {
    offset: i32 // 与GOTO相比索引从2字节变成了4字节
}

impl Instruction for GOTO_W {
    fn fetch_operands(&mut self, reader: &mut BytecodeReader) {
        self.offset = reader.read_i32();
    }

    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        base::branch(frame, self.offset)
    }
}