use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};


#[derive(Debug, Default)]
pub struct D2F {}

// Convert double to float
impl Instruction for D2F {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let d = stack.borrow_mut().pop_double();
        let f = d as f32;
        stack.borrow_mut().push_float(f);
    }
}

// Convert double to int
#[derive(Debug, Default)]
pub struct D2I {}

impl Instruction for D2I {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let d = stack.borrow_mut().pop_double();
        let i = d as i32;
        stack.borrow_mut().push_int(i);
    }
}

// Convert double to long
#[derive(Debug, Default)]
pub struct D2L {}

impl Instruction for D2L {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let d = stack.borrow_mut().pop_double();
        let l = d as i64;
        stack.borrow_mut().push_long(l);
    }
}