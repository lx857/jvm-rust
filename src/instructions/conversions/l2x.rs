use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};

// Convert long to double
#[derive(Debug, Default)]
pub struct L2D {}

impl Instruction for L2D {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let l = stack.borrow_mut().pop_long();
        let d = l as f64;
        stack.borrow_mut().push_double(d);
    }
}

// Convert long to float
#[derive(Debug, Default)]
pub struct L2F {}

impl Instruction for L2F {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let l = stack.borrow_mut().pop_long();
        let f = l as f32;
        stack.borrow_mut().push_float(f);
    }
}

// Convert long to int
#[derive(Debug, Default)]
pub struct L2I {}

impl Instruction for L2I {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let l = stack.borrow_mut().pop_long();
        let i = l as i32;
        stack.borrow_mut().push_int(i);
    }
}