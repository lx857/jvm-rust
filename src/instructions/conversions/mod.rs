mod d2x; // 把double变量强制转换成其他类型
mod f2x; // 把float变量强制转换成其他类型
mod i2x; // 把int变量强制转换成其他类型
mod l2x; // 把long变量强制转换成其他类型

pub use d2x::*;
pub use f2x::*;
pub use i2x::*;
pub use l2x::*;