use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};


#[derive(Debug, Default)]
pub struct F2D {}

// Convert float to double
impl Instruction for F2D {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let f = stack.borrow_mut().pop_float();
        let d = f as f64;
        stack.borrow_mut().push_double(d);
    }
}

// Convert float to int
#[derive(Debug, Default)]
pub struct F2I {}

impl Instruction for F2I {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let f = stack.borrow_mut().pop_float();
        let i = f as i32;
        stack.borrow_mut().push_int(i);
    }
}

// Convert float to long
#[derive(Debug, Default)]
pub struct F2L {}

impl Instruction for F2L {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let f = stack.borrow_mut().pop_float();
        let l = f as i64;
        stack.borrow_mut().push_long(l);
    }
}