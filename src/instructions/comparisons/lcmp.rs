use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};

    // 把栈顶的两个long变量弹出，进行比较，然后把比较结果（int型0、1或-1）推入栈顶

// Compare long
#[derive(Debug, Default)]
pub struct LCMP {}

impl Instruction for LCMP {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        let stack = frame.borrow().get_operand_stack();
        let v2 = stack.borrow_mut().pop_long();
        let v1 = stack.borrow_mut().pop_long();
        if v1 > v2 {
            stack.borrow_mut().push_int(1);
        } else if v1 == v2 {
            stack.borrow_mut().push_int(0);
        } else {
            stack.borrow_mut().push_int(-1);
        }
    }
}