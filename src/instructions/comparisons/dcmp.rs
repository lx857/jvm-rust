use std::{rc::Rc, cell::RefCell};

use crate::{instructions::base::Instruction, rtda::Frame};

// 用于比较double变量,与fcmp类似。

// Compare double
#[derive(Debug, Default)]
pub struct DCMPG {}

impl Instruction for DCMPG {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _dcmp(frame, true);
    }
}

#[derive(Debug, Default)]
pub struct DCMPL {}

impl Instruction for DCMPL {
    fn execute(&mut self, frame: Rc<RefCell<Frame>>) {
        _dcmp(frame, false);
    }
}

fn _dcmp(frame: Rc<RefCell<Frame>>, g_flag: bool) {
    let stack = frame.borrow().get_operand_stack();
    let v2 = stack.borrow_mut().pop_double();
    let v1=  stack.borrow_mut().pop_double();
    let r = if v1 > v2 { 1 } 
            else if v1 == v2 { 0 } 
            else if v1 < v2 { -1 } 
            else if g_flag { 1 } 
            else {0};
    stack.borrow_mut().push_int(r);
    
}