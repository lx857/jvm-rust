mod lcmp;    // 用于比较long变量
mod fcmp;    // 用于比较float变量
mod dcmp;    // 用于比较double变量
mod ifcond;  // 把操作数栈顶的int变量弹出，然后跟0进行比较，满足条件则跳转。
mod if_icmp; // 把栈顶的两个int变量弹出，然后进行比较，满足条件则跳转。
mod if_acmp; // 把栈顶的两个引用弹出，根据引用是否相同进行跳转。

pub use lcmp::*;
pub use fcmp::*;
pub use dcmp::*;
pub use ifcond::*;
pub use if_icmp::*;
pub use if_acmp::*;