/// 运行时数据区可以分为两类：一类是多线程共享的，另一类则是线程私有的。
/// 多线程共享的运行时数据区需要在Java虚拟机启动时创建好，在Java虚拟机退出时销毁。
/// 线程私有的运行时数据区则在创建线程时才创建，线程退出时销毁。

#[warn(non_upper_case_globals)]

mod thread;
mod stack;
mod frame;
mod slots;
mod local_vars;
mod operand_stack;
mod object;
pub mod heap;

pub use object::{Object, ExtraType, DataType};
pub use thread::Thread;
pub use stack::Stack;
pub use frame::Frame;
pub use local_vars::LocalVars;
pub use operand_stack::OperandStack;
pub use slots::{Slot, Slots};