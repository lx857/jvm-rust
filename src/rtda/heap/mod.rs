// 至于方法区到底位于何处，是固定大小还是动态调整，是否参与垃圾回收，以及如何在方法区内存放类数据等，Java虚拟机规范并没有明确规定。

mod access_flags;
mod class;
mod field;
mod method;
mod constant_pool;
mod cp_classref;
mod cp_fieldref;
mod cp_methodref;
mod cp_interface_methodref;
mod cp_invoke_dynamic;
mod class_loader;
mod method_lookup;
mod method_descriptor;
mod method_descriptor_parser;
mod array_object;
mod array_class;
mod class_name_helper;
pub mod string_pool;
mod exception_table;

pub use class::Class;
pub use method::Method;
pub use field::Field;

pub use constant_pool::Constant;
pub use constant_pool::ConstantPool;

pub use cp_classref::ClassRef;
pub use cp_fieldref::FieldRef;
pub use cp_methodref::MethodRef;
pub use cp_interface_methodref::InterfaceMethodRef;
pub use cp_invoke_dynamic::{InvokeDynamic, MethodHandle, MethodType};

pub use class_loader::ClassLoader;

pub use method_lookup::*;

pub use array_object::ArrayObject;

pub use exception_table::*;
