
#[derive(Default, Debug, Clone)]
pub struct MethodDescriptor {
    paramter_types: Vec<String>,
    return_type: String,
}

impl MethodDescriptor {
    pub fn add_parameter_type(&mut self, t: String) {
        // let p_len = self.paramter_types.len();
        self.paramter_types.push(t);
    }

    pub fn get_paramter_types(&self) -> &Vec<String> {
        &self.paramter_types
    }

    pub fn set_return_type(&mut self, return_type: String) {
        self.return_type = return_type
    }

    pub fn get_return_type(&self) -> &str {
        &self.return_type
    }
}