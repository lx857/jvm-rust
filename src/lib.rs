#![allow(warnings)]




use std::ptr::NonNull;

use std::panic::Location;  



  
fn get_location() -> String {  
    let location = Location::caller();  
    let file = location.file();  
    let line = location.line();  
    let col = location.column();  
    format!("File: {}, Line: {}, Column: {}", file, line, col)  
}

#[macro_export]
macro_rules! raw {
    ($x:expr) => { Box::into_raw(Box::new($x)) };
}

#[macro_export]
macro_rules! nonnull {
    ($x:expr) => { NonNull::new(Box::into_raw(Box::new($x))) };
}


static DEVELOP: bool = true;
// errmsg 用于测试时获取代码位置
#[macro_export]
macro_rules! errmsg {
    ($msg:literal) => {
        {
            let location = Location::caller();  
            let file = location.file();  
            let line = location.line();  
            let col = location.column();
            if DEVELOP {
                format!("Error Message: {} -> {} Line: {} Column: {}", $msg, file, line, col)
            } else {
                format!("{}", $msg)
            }
        }
    };
}







#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn lib() {
        // test_lib()
        let v = raw!((1,23));
        let nv = nonnull!((1,23));

        enum EE {E, e}
        let e = raw!(EE::e);
        dbg!(e);
        dbg!(v);
        let vv = vec![1,3,4];
    }

    #[test]
    fn location() {
        println!("{}", errmsg!("err"));
    }
}