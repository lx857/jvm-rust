use std::env;

#[derive(Debug, Clone, Default)]
pub struct Cmd {
    pub help_flag: bool,
    pub version_flag: bool,
    pub verbose_class_flag: bool,
    pub verbose_inst_falg: bool,
    pub cp_option: String,
    pub x_jre_opthin: String,
    pub class: String,
    pub args: Vec<String>,
}

impl Cmd {
    pub fn new() -> Cmd {
        Cmd::default()
    }

    // 处理参数
    pub fn parse_cmd() -> Cmd {
        let mut cmd = Cmd::new();

        // 命令行参数
        let args:Vec<String> = env::args().collect();

        // 第一个参数是当前程序
        let _program = args[0].clone();

        // 其他参数
        let mut other = vec![];
        // 处理参数
        let mut iter = args.into_iter().skip(1);
        while let Some(a) = iter.next() {
            match &a[..] {
                "-version" => cmd.version_flag = true,
                "?" | "-help" => cmd.help_flag = true,
                "-verbose" | "-verbose:class" => cmd.verbose_class_flag = true,
                "-verbose:inst" => cmd.verbose_inst_falg = true,
                "-cp" | "-classpath" => {
                    if let Some(next) = iter.next() {
                        cmd.cp_option = next.to_string();
                    } else {
                        panic!("No value specified for parameter -cp|--classpath.");
                    }
                },
                "-Xjre" => {
                    if let Some(next) = iter.next() {
                        cmd.x_jre_opthin = next.to_string();
                    } else {
                        panic!("No value specified for parameter -Xjre.");
                    }
                },
                _ => {
                    other.push(a);
                }
            }
        }

        // 处理其他参数
        if !other.is_empty() {
            cmd.class = other[0].clone();
            cmd.args = other.into_iter().skip(1).collect();
        }
        cmd
    }

    pub fn print_usage() {
        println!("
USAGE:
    jvm-rust [FLAGS] [class] [arg]

FLAGS:
    -?, -help          print help
    -version           print version and exit
    -cp,-classpath     classpath
    -Xjre              jre_path
");
    }
}



