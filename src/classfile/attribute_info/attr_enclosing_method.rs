use std::{rc::Rc, cell::RefCell};

use crate::classfile::{ConstantPool, ClassReader};

use super::AttributeInfo;


// EnclosingMethod_attribute {
// u2 attribute_name_index; // 属性名称索引
// u4 attribute_length; // 必须是4
// u2 class_index; // 指向CONSTANT_Class_info结构，表示包含当前类声明的最内层类。
// u2 method_index; // 指向CONSTANT_NameAndType_info结构，表示类中由上述class_index属性引用的方法的名称和类型。
// }

// 当 它 表 示 一 个 本 地 类 或 一 个匿名类, 必须有一个EnclosingMethod属 性
// ClassFile结 构 的 属 性 表 中 最 多 只 能 有 一 个 EnclosingMethod属 性

// 如果当前类没有立即被方法或构造函数包围，那么method_index项的值必须为零。

#[derive(Clone)]
pub struct EnclosingMethodAttribute {
    cp: Rc<RefCell<ConstantPool>>,
    class_index: usize,
    method_index: usize,
}

impl AttributeInfo for EnclosingMethodAttribute {
    fn read_info(&mut self, reader: &mut ClassReader) {
        self.class_index = reader.read_u16().into();
        self.method_index = reader.read_u16().into();
    }
}

impl EnclosingMethodAttribute {
    pub fn new(cp: Rc<RefCell<ConstantPool>>) -> EnclosingMethodAttribute {
        EnclosingMethodAttribute { 
            cp,
            class_index: usize::default(),
            method_index: usize::default(),
        }
    }

    pub fn class_name(&self) -> String {
        self.cp.borrow().get_class_name(self.class_index)
    }

    pub fn method_name_and_type(&self) -> (String, String) {
        self.cp.borrow().get_name_and_type(self.method_index)
    }


}