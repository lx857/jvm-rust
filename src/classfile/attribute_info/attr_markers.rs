use super::{AttributeInfo, ClassReader};


// Deprecated_attribute {
// u2 attribute_name_index; // 常量池中属性名称索引
// u4 attribute_length; // 必须为0x00000000
// }

// Deprecated属性用于表示某个类、字段或者方法，已经被程序作者定为不再推荐使用，
// 它可以通过代码中使用“@deprecated”注解进行设置。
pub struct DeprecatedAttribute {

}

impl AttributeInfo for DeprecatedAttribute {
    #[allow(unused_variables)]
    fn read_info(&mut self, reader: &mut ClassReader) {
        // read nothing
    }
}

impl DeprecatedAttribute {
    pub fn new() -> DeprecatedAttribute {
        DeprecatedAttribute {  }
    }
}

// Synthetic_attribute {
// u2 attribute_name_index; // 常量池中属性名称索引
// u4 attribute_length; // 必须为0x00000000
// }

// Synthetic属性代表此字段或者方法并不是由Java源码直接产生的，而是由编译器自行添加的
// 在JDK 5之后，标识一个类、字段或者方法是编译器自动产生的，也可以设置它们访问标志中的ACC_SYNTHETIC标志位。

// 所有由不属于用户代码产生的类、方法及字段都应当至少设置Synthetic属性或者
// ACC_SYNTHETIC标志位中的一项，唯一的例外是实例构造器“<init>()”方法和类构造器“<clinit>()”方法。
pub struct SyntheticAttribute {

}

impl AttributeInfo for SyntheticAttribute {
    #[allow(unused_variables)]
    fn read_info(&mut self, reader: &mut ClassReader) {
        // read nothing
    }
}

impl SyntheticAttribute {
    pub fn new() -> SyntheticAttribute {
        SyntheticAttribute {  }
    }
}
