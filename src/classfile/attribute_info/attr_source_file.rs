use std::{rc::Rc, cell::RefCell};

use super::{ConstantPool, ClassReader, AttributeInfo};


// SourceFile_attribute {
//     u2 attribute_name_index; // 属性名称索引
//     u4 attribute_length; // 必须是2，应该是读取sourcefile_index需要两个长度
//     u2 sourcefile_index; // 指向CONSTANT_Utf8_info，源码的文件名
// }

// 用于记录生成这个Class文件的源码文件名称。

// 默认生成，可在javac中通过使用-g：none或-g：source选项来取消或要求生成这项信息。

// 在Java中，对于大多数的类来说，类名和文件名是一致的，但是有一些特殊情况（如内部类）例外。
// 如果不生成这项属性，当抛出异常时，堆栈中将不会显示出错代码所属的文件名。

pub struct SourceFileAttribute {
    cp: Rc<RefCell<ConstantPool>>,
    source_file_index: usize
}

impl AttributeInfo for SourceFileAttribute {
    fn read_info(&mut self, reader: &mut ClassReader) {
        self.source_file_index = reader.read_u16().into()
    }
}

impl SourceFileAttribute {
    pub fn new(cp: Rc<RefCell<ConstantPool>>) -> SourceFileAttribute {
        SourceFileAttribute { 
            cp, 
            source_file_index: usize::default()
        }
    }

    pub fn file_name(&self) -> String {
        self.cp.borrow().get_utf8(self.source_file_index)
    }
}