use super::{AttributeInfo, ClassReader};


// 存储未识别的属性
pub struct UnparsedAttribute {
    name: String,
    length: usize,
    info: Vec<u8>
}

impl AttributeInfo for UnparsedAttribute {
    fn read_info(&mut self, reader: &mut ClassReader) {
        self.info = reader.read_bytes(self.length)
    }
}

impl UnparsedAttribute {
    pub fn new(name: String, length: usize) -> UnparsedAttribute {
        UnparsedAttribute { name, length, info: vec![] }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn info(&self) -> Vec<u8> {
        self.info.clone()
    }
}