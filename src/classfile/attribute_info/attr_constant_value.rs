use super::{AttributeInfo, ClassReader};

// ConstantValue_attribute {
// u2 attribute_name_index; // 属性名称索引
// u4 attribute_length; // 必须是2
// u2 constantvalue_index; // 指向常量池中long、float、double、integer、string中的一种
// }

// ConstantValue属性是field_info结构(§4.5)的属性表中的⼀个固定⻓度的属性。

// 作用：通知虚拟机给静态变量赋值
// 只有被static关键字修饰的变量（类变量）才可以使用这项属性。

// 对非static类型的变量（也就是实例变量）的赋值是在实例构造器<init>()方法中进行的；
// 而对于类变量，则有两种方式可以选择：在类构造器<clinit>()方法中或者使用ConstantValue属性。

pub struct ConstantValueAttribute {
    constant_value_index: usize
}

impl AttributeInfo for ConstantValueAttribute {
    fn read_info(&mut self, reader: &mut ClassReader) {
        self.constant_value_index = reader.read_u16().into()
    }
}

impl ConstantValueAttribute {
    pub fn new() -> ConstantValueAttribute {
        ConstantValueAttribute { constant_value_index: usize::default() }
    }

    pub fn constant_value_index(&self) -> usize {
        self.constant_value_index
    }
}