use super::AttributeInfo;

// Exceptions_attribute {
// u2 attribute_name_index;
// u4 attribute_length;
// u2 number_of_exceptions; // 表示可能抛出多少种异常
// u2 exception_index_table[number_of_exceptions]; // 指向CONSTANT_Class_info，代表异常类型
// }

// Exceptions属性
// 作用：列举出方法中可能抛出的受查异常（Checked Excepitons），也就是方法描述时在throws关键字后面列举的异常。
#[derive(Default, Clone)]
pub struct ExceptionsAttribute {
    exception_index_table: Vec<u16>
}

impl AttributeInfo for ExceptionsAttribute {
    fn read_info(&mut self, reader: &mut crate::classfile::ClassReader) {
        self.exception_index_table = reader.read_u16s();
    }
}

impl ExceptionsAttribute {
    pub fn new() -> ExceptionsAttribute {
        ExceptionsAttribute { exception_index_table: vec![] }
    }

    pub fn exception_index_table(&self) -> &Vec<u16> {
        &self.exception_index_table
    }
}
