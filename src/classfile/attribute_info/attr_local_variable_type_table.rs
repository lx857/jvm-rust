use super::AttributeInfo;


// LocalVariableTypeTable_attribute {
//     u2 attribute_name_index;
//     u4 attribute_length;
//     u2 local_variable_type_table_length;
//     {   u2 start_pc;
//         u2 length;
//         u2 name_index;
//         u2 signature_index; // 特征签名
//         u2 index;
//     } local_variable_type_table[local_variable_type_table_length];
// }

// 与LocalVariableTable非常相似，仅仅是把记录的字段描述符的descriptor_index替换成了字段的特征签名（Signature）。

// 对于非泛型类型来说，描述符和特征签名能描述的信息是能吻合一致的，
// 但是泛型引入之后，由于描述符中泛型的参数化类型被擦除掉，描述符就不能准确描述泛型类型了。
// 因此出现了LocalVariableTypeTable属性，使用字段的特征签名来完成泛型的描述。

pub struct LocalVariableTypeTableAttribute {
    local_variable_table: Vec<LocalVariableTypeTableEntry>
}

impl AttributeInfo for LocalVariableTypeTableAttribute {
    fn read_info(&mut self, reader: &mut crate::classfile::ClassReader) {
        let line_number_table_length = reader.read_u16();
        for _ in 0..line_number_table_length {
            self.local_variable_table.push(
                LocalVariableTypeTableEntry { 
                    start_pc: reader.read_u16(), 
                    length: reader.read_u16(), 
                    name_index: reader.read_u16(), 
                    signature_index: reader.read_u16(), 
                    index: reader.read_u16()
                }
            )
        }
    }
}

impl LocalVariableTypeTableAttribute {
    pub fn new() -> LocalVariableTypeTableAttribute {
        LocalVariableTypeTableAttribute { local_variable_table: vec![] }
    }
}

#[allow(warnings)]
pub struct LocalVariableTypeTableEntry {
    start_pc: u16,
    length: u16,
    name_index: u16,
    signature_index: u16,
    index: u16
}