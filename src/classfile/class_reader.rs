

pub struct ClassReader {
    data: Vec<u8>
}

impl ClassReader {

    pub fn new(data: Vec<u8>) -> ClassReader {
        ClassReader { data }
    }

    pub fn read_u8(&mut self) -> u8 {
        self.data.drain(..1).last().unwrap()
    }

    pub fn read_u16(&mut self) -> u16 {
        let v: Vec<u8> = self.data.drain(..2).collect();
        u16::from_be_bytes([v[0], v[1]])
        
    }

    pub fn read_u32(&mut self) -> u32 {
        let v: Vec<u8> = self.data.drain(..4).collect();
        u32::from_be_bytes([v[0], v[1], v[2], v[3]])
    }

    pub fn read_u64(&mut self) -> u64 {        
        let v: Vec<u8> = self.data.drain(..8).collect();
        u64::from_be_bytes([v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7]])
    }

    pub fn read_u16s(&mut self) -> Vec<u16> {
        (0..self.read_u16()).map(|_| self.read_u16()).collect()
    }

    pub fn read_bytes(&mut self, length: usize) -> Vec<u8> {
        self.data.drain(0..length).collect()
    }
}