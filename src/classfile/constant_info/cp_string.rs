use std::{cell::RefCell, rc::Rc};
use super::{ConstantInfo, ClassReader, ConstantPool};

// CONSTANT_String_info {
// u1 tag; // 值为8
// u2 string_index; // 指向常量池中字符串索引
// }
pub struct ConstantStringInfo {
    cp: Rc<RefCell<ConstantPool>>,
    string_index: usize
}

impl ConstantInfo for ConstantStringInfo {
    fn read_info(&mut self, reader: &mut ClassReader){
        self.string_index = reader.read_u16().into()
    }
}

impl ConstantStringInfo {
    pub fn new(cp: Rc<RefCell<ConstantPool>>) -> ConstantStringInfo {
        ConstantStringInfo { 
            cp, 
            string_index: usize::default()
        }
    }

    pub fn get_string(&self) -> String {
        self.cp.borrow().get_utf8(self.string_index)
    }
}