use std::{rc::Rc, cell::RefCell};

use super::{ConstantInfo, ClassReader, ConstantPool};

// CONSTANT_Class_info {
// u1 tag; // 值为7
// u2 name_index; // 指向常量池中全限定类名索引
// }
#[derive(Clone)]
pub struct ConstantClassInfo {
    cp: Rc<RefCell<ConstantPool>>,
    name_index: usize
}

impl ConstantInfo for ConstantClassInfo {
    fn read_info(&mut self, reader: &mut ClassReader){
        self.name_index =  reader.read_u16().into();
    }
}

impl ConstantClassInfo {
    pub fn new(cp: Rc<RefCell<ConstantPool>>) -> ConstantClassInfo {
        ConstantClassInfo { 
            cp, 
            name_index: usize::default() 
        }
    }

    pub fn name_index(&self) -> usize {
        self.name_index
    }

    pub fn get_name(&self) -> String {
        self.cp.borrow().get_utf8(self.name_index)
    }
}