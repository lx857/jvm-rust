use super::{ConstantInfo, ClassReader};

/// CONSTANT_Integer_info {
/// u1 tag; // 值为3
/// u4 bytes;
/// }
pub struct ConstantIntegerInfo {
    val: i32
}

impl ConstantInfo for ConstantIntegerInfo {
    fn read_info(&mut self, reader: &mut ClassReader){
        self.val = reader.read_u32() as i32
    }
}

impl ConstantIntegerInfo {
    pub fn new() -> ConstantIntegerInfo {
        ConstantIntegerInfo { val: i32::default() }
    }

    pub fn get_val(&self) -> i32 {
        self.val
    }
}


/// CONSTANT_Float_info {
/// u1 tag; // 值为4
/// u4 bytes;
/// }
pub struct ConstantFloatInfo {
    val: f32
}

impl ConstantInfo for ConstantFloatInfo {
    fn read_info(&mut self, reader: &mut ClassReader){
        self.val = f32::from_bits(reader.read_u32())
    }
}

impl ConstantFloatInfo {
    pub fn new() -> ConstantFloatInfo {
        ConstantFloatInfo { val: f32::default() }
    }
    
    pub fn get_val(&self) -> f32 {
        self.val
    }
}

/// CONSTANT_Long_info {
/// u1 tag; // 值为5
/// u4 high_bytes;
/// u4 low_bytes; 
/// }
pub struct ConstantLongInfo {
    val: i64
}

impl ConstantInfo for ConstantLongInfo {
    fn read_info(&mut self, reader: &mut ClassReader){
        self.val = reader.read_u64() as i64
    }
}

impl ConstantLongInfo {
    pub fn new() -> ConstantLongInfo {
        ConstantLongInfo { val: i64::default() }
    }
    
    pub fn get_val(&self) -> i64 {
        self.val
    }
}

/// CONSTANT_Double_info {
/// u1 tag; // 值为6
/// u4 high_bytes;
/// u4 low_bytes; 
/// }
pub struct ConstantDoubleInfo{
    val: f64
}

impl ConstantInfo for ConstantDoubleInfo {
    fn read_info(&mut self, reader: &mut ClassReader){
        self.val = f64::from_bits(reader.read_u64());
    }
}

impl ConstantDoubleInfo {
    pub fn new() -> ConstantDoubleInfo {
        ConstantDoubleInfo { val: f64::default() }
    }
    
    pub fn get_val(&self) -> f64 {
        self.val
    }
}