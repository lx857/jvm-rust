use super::ConstantInfo;
use super::ClassReader;

// CONSTANT_NameAndType_info {
// u1 tag; // 值为12
// u2 name_index; // 指向改字段或方法的常量项的索引
// u2 descriptor_index; // 指向改字段或方法的常量项的索引
// }
pub struct ConstantNameAndTypeInfo {
    name_index: usize,
    descriptor_index: usize
}

impl ConstantInfo for ConstantNameAndTypeInfo {
    fn read_info(&mut self, reader: &mut ClassReader){
        self.name_index = reader.read_u16().into();
        self.descriptor_index = reader.read_u16().into();
    }
}

impl ConstantNameAndTypeInfo {
    pub fn new() -> ConstantNameAndTypeInfo {
        ConstantNameAndTypeInfo { 
            name_index: usize::default(), 
            descriptor_index: usize::default() 
        }
    }

    pub fn name_index(&self) -> usize {
        self.name_index
    }

    pub fn descriptor_index(&self) -> usize {
        self.descriptor_index
    }
}