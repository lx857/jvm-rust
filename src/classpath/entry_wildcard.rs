use super::{CompositeEntry, ZipEntry, Entry};
use std::fs;

pub fn new(path: &str) -> CompositeEntry {
    let base_dir = &path[..path.len() - 1];
    let mut entrys: Vec<Box<dyn Entry>> = vec![];
    let dir_res = fs::read_dir(base_dir);
    match dir_res {
        Ok(dir) => {
            for dir_entry in dir {
                match dir_entry {
                    Ok(dir_entry) => {
                        let path = dir_entry.path();
                        if path.is_dir() {
                            continue;
                        }
                        let path_str = path.to_str().unwrap();
                        if path_str.ends_with(".jar") || path_str.ends_with(".JAR") {
                            entrys.push(Box::new(ZipEntry::new(path_str)));
                        }
                    },
                    Err(e) => panic!("{e}")
                }
            }
        },
        Err(e) => panic!("{e}")
    }
    CompositeEntry::from(entrys)
}