use std::error::Error;

use super::{DirEntry, CompositeEntry, ZipEntry, WildcardEntry};
use std::fmt;

#[cfg(target_os = "windows")]
pub const PATH_LIST_SAPARATOR: char = ';'; 

#[cfg(target_os = "linux")]
pub const PATH_LIST_SAPARATOR: char = ':';

// 路径分隔符
#[cfg(target_os = "macos")]
pub const PATH_LIST_SAPARATOR: char = ':';

// 实现Diaplsy 父trait
pub trait Entry: fmt::Display  {
    fn read_class(&self, class_name: &str) -> Result<Vec<u8>, Box<dyn Error>>;
    fn string(&self) -> String;
}

pub fn new_entry(path: &str) -> Box<dyn Entry> {
    if path.contains(PATH_LIST_SAPARATOR) {
        // 存在路径分隔符
        Box::new(CompositeEntry::new(path))
    } else if path.ends_with("*") {
        // 匹配全部
        Box::new(WildcardEntry::new(&path))
    } else if path.ends_with(".jar") || path.ends_with(".JAR") || 
              path.ends_with(".zip") || path.ends_with(".ZIP") {
        // 为压缩文件
        Box::new(ZipEntry::new(&path))
    } else {
        // 默认
        Box::new(DirEntry::new(&path))
    }
}