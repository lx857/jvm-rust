use std::{rc::Rc, cell::RefCell};

use crate::{native, rtda::Frame};

pub fn init() {
    native::register("sun/io/Win32ErrorMode", "setErrorMode", "(J)J", set_error_mode);
}

// public native String intern();
// ()Ljava/lang/String;
fn set_error_mode(frame: Rc<RefCell<Frame>>) {
    frame.borrow().get_operand_stack().borrow_mut().push_long(0);
}