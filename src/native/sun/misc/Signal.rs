use std::{rc::Rc, cell::RefCell};

use crate::{native::{self, NativeMethod}, rtda::{Frame, heap::string_pool}};

pub fn init() {
    _signal(find_signal, "findSignal", "(Ljava/lang/String;)I");
	_signal(handle0, "handle0", "(IJ)J");
}

fn _signal(method: NativeMethod, name: &str, desc: &str) {
	native::register("sun/misc/Signal", name, desc, method)
}

// private static native int findSignal(String string);
// (Ljava/lang/String;)I
fn find_signal(frame: Rc<RefCell<Frame>>) {
    let vars = frame.borrow().get_local_vars();
    let _ = vars.borrow().get_ref(0); // name

    let stack = frame.borrow().get_operand_stack();
    stack.borrow_mut().push_int(0); // todo
}

// private static native long handle0(int i, long l);
// (IJ)J
fn handle0(frame: Rc<RefCell<Frame>>) {
    let vars = frame.borrow().get_local_vars();
    let _ = vars.borrow().get_int(0);
    let _ = vars.borrow().get_long(1);

    let stack = frame.borrow().get_operand_stack();
    stack.borrow_mut().push_long(0); // todo
}

// private static native void raise0(int i);
