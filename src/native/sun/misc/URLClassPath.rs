use std::{rc::Rc, cell::RefCell};

use crate::{native, rtda::Frame};

pub fn init() {	
    native::register("sun/misc/URLClassPath", "getLookupCacheURLs", "(Ljava/lang/ClassLoader;)[Ljava/net/URL;", get_lookup_cache_urls);
}

// private static native URL[] getLookupCacheURLs(ClassLoader var0);
// (Ljava/lang/ClassLoader;)[Ljava/net/URL;
fn get_lookup_cache_urls(frame: Rc<RefCell<Frame>>) {
    frame.borrow().get_operand_stack().borrow_mut().push_ref(None);
}