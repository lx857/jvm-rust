use std::{rc::Rc, cell::RefCell};

use crate::{native, rtda::Frame};

static FD: &'static str = "java/io/FileDescriptor";

pub fn init() {
    // native::register("java/io/FileDescriptor", "initIDs", "()V", init_ids);
    native::register(FD, "set", "(I)J", set);
}

// private static native void initIDs();
// ()V
fn init_ids(frame: Rc<RefCell<Frame>>) {
    // do nothing
}

// private static native long set(int d);
// (I)J
fn set(frame: Rc<RefCell<Frame>>) {
    // todo
    frame.borrow().get_operand_stack().borrow_mut().push_long(0);
}