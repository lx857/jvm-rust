use std::{rc::Rc, cell::RefCell};

use crate::{native, rtda::Frame};

pub fn init() {
    // native::register("java/io/FileInputStream", "initIDs", "()V", init_ids);
}

// private static native void initIDs();
// ()V
fn init_ids(frame: Rc<RefCell<Frame>>) {
    // static methods to store field ID's in initializers 
    // JNIEXPORT void JNICALL
    // Java_java_io_FileInputStream_initIDs(JNIEnv *env, jclass fdClass) {
    //     fis_fd = (*env)->GetFieldID(env, fdClass, "fd", "Ljava/io/FileDescriptor;");
    // }
    
    // 上面是jdk源码中找到的，注释是在初始化程序中存储字段ID的静态方法
    // 搜了一下 (*env)->GetFieldID 是 JNI (Java Native Interface) 的一部分，是用来获取这个字段 ID 的
    // 未进行返回，目前涉及初始化，暂不做任何操作
}