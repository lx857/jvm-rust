use std::{rc::Rc, cell::RefCell};

use crate::{native, rtda::{Frame, heap::string_pool}};

pub fn init() {
    native::register("java/util/concurrent/atomic/AtomicLong", "VMSupportsCS8", "()Z", vm_supports_cs8)
}

fn vm_supports_cs8(frame: Rc<RefCell<Frame>>) {
    frame.borrow().get_operand_stack().borrow_mut().push_boolean(false);
}