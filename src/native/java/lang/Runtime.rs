use std::{rc::Rc, cell::RefCell};

use crate::{native, rtda::Frame};

pub fn init() {
    native::register("java/lang/Runtime", "availableProcessors", "()I", available_processors);
}

// public native int availableProcessors();
// ()I
fn available_processors(frame: Rc<RefCell<Frame>>) {
    let num_cpus = num_cpus::get();

    let stack = frame.borrow().get_operand_stack();
    stack.borrow_mut().push_int(num_cpus as i32);
}