use std::{rc::Rc, cell::RefCell};

use crate::{native, rtda::{Frame, heap::string_pool}};

pub fn init() {
    native::register("java/lang/String", "intern", "()Ljava/lang/String;", intern);
}

// public native String intern();
// ()Ljava/lang/String;
fn intern(frame: Rc<RefCell<Frame>>) {
    // 获取this引用，等同于get_ref(0)
    let this = frame.borrow().get_local_vars().borrow().get_this().unwrap();
    let interned = string_pool::intern_string(this);
    frame.borrow().get_operand_stack().borrow_mut().push_ref(Some(interned));
}