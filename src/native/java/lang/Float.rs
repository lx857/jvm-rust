use std::{rc::Rc, cell::RefCell};

use crate::{native, rtda::Frame};

pub fn init() {
    native::register("java/lang/Float", "floatToRawIntBits", "(F)I", float_to_raw_int_bits);
    native::register("java/lang/Float", "intBitsToFloat", "(I)F", int_bits_to_float);
}

// public static native int floatToRawIntBits(float value);
// (F)I
fn float_to_raw_int_bits(frame: Rc<RefCell<Frame>>) {
    let value = frame.borrow().get_local_vars().borrow().get_float(0);
    let bits = value.to_be_bytes();
    frame.borrow().get_operand_stack().borrow_mut().push_int(i32::from_be_bytes(bits));
}

// public static native float intBitsToFloat(int bits);
// (I)F
fn int_bits_to_float(frame: Rc<RefCell<Frame>>) {
    let bits = frame.borrow().get_local_vars().borrow().get_int(0);
    let value = bits.to_be_bytes();
    frame.borrow().get_operand_stack().borrow_mut().push_float(f32::from_be_bytes(value));
}