use std::{rc::Rc, cell::RefCell};

use crate::{native, rtda::Frame};

pub fn init() {
    native::register("java/lang/Double", "doubleToRawLongBits", "(D)J", double_to_raw_lang_bits);
    native::register("java/lang/Double", "longBitsToDouble", "(J)D", lang_bits_to_double);
}

// public static native long doubleToRawLongBits(double value);
// (D)J
fn double_to_raw_lang_bits(frame: Rc<RefCell<Frame>>) {
    let value = frame.borrow().get_local_vars().borrow().get_double(0);
    let bits = value.to_be_bytes();
    frame.borrow().get_operand_stack().borrow_mut().push_long(i64::from_be_bytes(bits));
}

// public static native double longBitsToDouble(long bits);
// (J)D
fn lang_bits_to_double(frame: Rc<RefCell<Frame>>) {
    let bits = frame.borrow().get_local_vars().borrow().get_long(0);
    let value = bits.to_be_bytes();
    frame.borrow().get_operand_stack().borrow_mut().push_double(f64::from_be_bytes(value));
}
