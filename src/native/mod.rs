#![allow(warnings)]


mod registry;
mod java {
    pub mod io {
        pub mod FileOutputStream;
        pub mod FileInputStream;
        pub mod FileDescriptor;
        pub mod UnixFileSystem;
    }
    pub mod lang {
        pub mod invoke {
            pub mod MethodHandleNatives;
            pub mod MethodHandle;
        }
        pub mod reflect {
            pub mod Array;
        }
        pub mod Object;
        pub mod Class;
        pub mod System;
        pub mod Float;
        pub mod Double;
        pub mod String;
        pub mod Thread;
        pub mod Throwable;
        pub mod Runtime;
        pub mod ClassLoader;
    }
    pub mod security {
        pub mod AccessController;
    }
    pub mod util {
        pub mod concurrent {
            pub mod atomic {
                pub mod AtomicLong;
            }
        }
    }
}
mod sun {
    pub mod io {
        pub mod Win32ErrorMode;
    }
    pub mod misc {
        pub mod VM;
        pub mod Unsafe;
        pub mod Unsafe_mem;
        pub mod malloc;
        pub mod Signal;
        pub mod URLClassPath;
    }
    pub mod reflect {
        pub mod Reflection;
        pub mod NativeConstructorAccessorImpl;
    }
}

pub use registry::*;
pub use java::lang::Throwable::StackTraceElement;
pub use sun::misc::malloc::*;
pub use java::lang::Thread::THREADS;

pub fn init() {
    java::io::FileOutputStream::init();
    java::io::FileInputStream::init();
    java::io::FileDescriptor::init();
    java::io::UnixFileSystem::init();
    java::lang::Object::init();
    java::lang::Class::init();
    java::lang::System::init();
    java::lang::Float::init();
    java::lang::Double::init();
    java::lang::String::init();
    java::lang::Thread::init();
    java::lang::Throwable::init();
    java::lang::Runtime::init();
    java::lang::reflect::Array::init();
    java::lang::ClassLoader::init();
    java::lang::invoke::MethodHandleNatives::init();
    java::lang::invoke::MethodHandle::init();
    java::security::AccessController::init();
    java::util::concurrent::atomic::AtomicLong::init();
    sun::io::Win32ErrorMode::init();
    sun::misc::VM::init();
    sun::misc::Unsafe::init();
    sun::misc::Unsafe_mem::init();
    sun::misc::Signal::init();
    sun::misc::URLClassPath::init();
    sun::reflect::Reflection::init();
    sun::reflect::NativeConstructorAccessorImpl::init();
}