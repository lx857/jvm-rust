#[cfg(test)]
mod java_tests {
    use super::super::run;

    #[test]
    // #[ignore]
    fn operation() {
        run("code/Operation");
    }

    #[test]
    // #[ignore]
    fn converter() {
        run("code/Converter");
    }
}