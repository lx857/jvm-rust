#![allow(warnings)]
use std::env;
use std::path::Path;
use std::rc::Rc;
use std::cell::RefCell;
use std::time::{Duration, SystemTime};
use std::thread::sleep;
use std::any::Any;


struct A {
    vec: Vec<Box<dyn B>>
}

impl A {
    pub fn add(&mut self, b: Box<dyn B>) {
        self.vec.push(b)
    }
}

trait B {
    fn name(&self) -> String;
    fn get(&self, i: usize) -> String;
}

struct B1 {
    name: String
}

impl B for B1 {
    fn name(&self) -> String {
        self.name.clone()
    }
    fn get(&self, i: usize) -> String {
        "".into()
    }
}

struct B2 {
    a: Rc<RefCell<A>>,
    name: String
}

impl B for B2 {
    fn name(&self) -> String {
        self.name.clone()
    }
    fn get(&self, i: usize) -> String {
        self.a.borrow().vec[i].name()
    }
}

struct B3 {
    a: Rc<RefCell<A>>,
    name: String
}

impl B for B3 {
    fn name(&self) -> String {
        self.name.clone()
    }
    fn get(&self, i: usize) -> String {
        self.a.borrow().vec[i].name()
    }
}

impl B3 {
    pub fn get(&self, i: usize) -> String {
        self.a.borrow().vec[i].name()
    }
}

pub struct Int {
    pub val: i32
}

impl Int {
    pub fn as_any(&self) -> &dyn Any {
        self
    }

    // pub fn any(&mut self) -> dyn Any {
    //     self
    // }
}



#[cfg(test)]
mod tests {

    use std::{any::Any, collections::HashMap, time::{UNIX_EPOCH, Instant}, sync::Arc, io::{self, Write}, fs::metadata};

    use crate::{classfile::ClassReader, classpath::DirEntry, rtda::heap::{Class, ArrayObject}};

    use super::*;

    #[test]
    fn test() {
        // assert!(1 == 1, "panic!!!!");
        // panic!("ppppp");
        // let mut map: HashMap<i32, &str> = HashMap::from_iter(vec![(1, "a"), (2, "b"), (3, "c")].into_iter());


        // for (key, value) in &map {
        //     println!("{}: {}", key, value);
        // }
        // let s = "Vbcdef";
        // assert_eq!(&s[..1], "V");
        // assert_ne!(&s[..1], "a");
        // let a = Arc::new(0);
        // let b = *a + 1;
        // dbg!(*a);
        // let cla = "Testaaa.java";
        // assert!(cla.ends_with(".java"));
        // assert_eq!(&cla[..cla.len() - 5], "Testaaa");
        // dbg!(100899404 * 31);

        // let s = " ".encode_utf16().collect::<Vec<u16>>();
        // let a = "À\u{1}\u{000}".encode_utf16().collect::<Vec<u16>>();
        // dbg!(s);
        // dbg!(a);
        // println!("\u{1}");
        // let now = Instant::now();
        // // let nons = n
        // let nanoseconds = now.elapsed().as_nanos(); 
        // dbg!(nanoseconds); 
        // let now = SystemTime::now();
        // let epoch = UNIX_EPOCH;

        // // Convert SystemTime to Duration and then to milliseconds
        // let duration = now.duration_since(epoch).unwrap();
        // // let nanos = duration.as_nanos() as i64;
        // dbg!(duration.as_nanos());
        // dbg!(duration.as_millis());
        let vec = vec![1,2,3,4,5];
        let rev = vec.into_iter().rev().collect::<Vec<i32>>();
        dbg!(rev);

    }

    #[test]
    fn panic_hock() {
        use std::panic;

        // panic::set_hook(Box::new(|_| {
        //     println!("Custom panic hook");
        // }));

        //注册一个自定义的 panic hook，替换之前注册的 hook。
        // 当线程发生 panic 时，但在调用 panic 运行时之前，会调用 panic 钩子。
        panic::set_hook(Box::new(|panic_info| {
            if let Some(s) = panic_info.payload().downcast_ref::<&str>() {
                println!("panic occurred: {s:?}");
            } else {
                println!("panic occurred");
            }
        }));

        panic!("Normal panic");
    }

    #[test]
    fn trhead_test_lock() {  
        let (sender, receiver) = channel();  
        let lock: Arc<Mutex<()>> = Arc::new(Mutex::new(()));
        let flag: Arc<bool> = Arc::new(true);  
        let sender1 = sender.clone();  
        let flag1 = flag.clone();
        let lock1 = lock.clone();
        // 创建第一个线程  
        thread::spawn(move || {  
            thread::sleep(Duration::from_secs(1));
            if *flag {
                let lk = lock.lock().unwrap();
                if *flag {
                    thread::sleep(Duration::from_secs(1));
                    println!("1 init");
                    unsafe {  
                        let ptr = flag.as_ref() as *const bool as *mut bool;
                        *ptr = false;
                    }  
                }
            }

            sender.send(()).unwrap(); // 发送一个信号给主线程  
            println!("1线程 stop")
            // drop(sender1);
        });  

        // 创建第2个线程  
        thread::spawn(move || {  
            // thread::sleep(Duration::from_secs(1));
            if *flag1 {
                let lk = lock1.lock().unwrap();
                if *flag1 {
                    thread::sleep(Duration::from_secs(1));
                    println!("2 init");
                    unsafe {  
                        let ptr = flag1.as_ref() as *const bool as *mut bool;
                        *ptr = false;
                    }  
                }
                
            }


            sender1.send(()).unwrap(); // 发送一个信号给主线程  
            println!("2线程 stop")
            // drop(sender1);
        });  
        // drop(sender); // 释放sender，以便接收到信号后退出主线程  
        for _ in receiver {

        }
        // 等待两个线程完成工作并接收到信号  
        // dbg!(counter.lock().unwrap());  
    }
        
    use std::sync::mpsc::channel;
    use std::thread;  
    use std::sync::Mutex;  
    #[test]
    fn trhead_test() {  
        let (sender, receiver) = channel();  
        let counter: Arc<Mutex<i32>> = Arc::new(Mutex::new(0));  
    
        // 创建第一个线程  
        // let sender1: std::sync::mpsc::Sender<()> = sender.clone();  
        let counter1 = counter.clone();  
        thread::spawn(move || {  
            for _ in 0..5 {  
                let mut count = counter1.lock().unwrap();  
                *count += 1;  
                println!("Thread 1: Counter = {}", *count);  
                new_thread1(sender.clone(), counter1.clone());  
            }  
            sender.send(()).unwrap(); // 发送一个信号给主线程  
            // drop(sender1);
        });  
        // drop(sender); // 释放sender，以便接收到信号后退出主线程  
        for _ in receiver {

        }
        // 等待两个线程完成工作并接收到信号  
        // dbg!(counter.lock().unwrap());  
    }

    fn new_thread1(sender2: std::sync::mpsc::Sender<()>, counter2: Arc<Mutex<i32>>) {
        // 动态增加线程  
        thread::spawn(move || {  
            for _ in 0..5 {  
                let mut count = counter2.lock().unwrap();  
                *count += 1;  
                println!("Thread 2: Counter = {}", *count);  
                new_thread2(sender2.clone(), counter2.clone());
            }  
            sender2.send(()).unwrap(); // 发送一个信号给第一个线程
            drop(sender2);  
        });
    }

    fn new_thread2(sender2: std::sync::mpsc::Sender<()>, counter2: Arc<Mutex<i32>>) {
        // 动态增加线程  
        thread::spawn(move || {  
            for _ in 0..5 {  
                let mut count = counter2.lock().unwrap();  
                *count += 1;  
                println!("Thread 3: Counter = {}", *count);  
            }  
            
            sender2.send(()).unwrap(); // 发送一个信号给第一个线程
            drop(sender2);  
        });
    }

    pub struct O(pub i32);
    impl O {
        pub fn v(&self) -> i32 {
            self.0
        }
    }

    #[test]
    fn class_loader() {
        struct Loader {
            val: i32,
            pub ptr: Option<*mut Loader>
        };
        impl Loader {
            pub fn abc(&mut self) -> i32 {
                self.val += 1;
                self.ptr = Some(self);
                self.val
            }
        }
        let loader = Box::into_raw(Box::new(Loader{val: 0, ptr: None}));
        dbg!(loader);
        assert_eq!(unsafe { &mut *loader }.abc(), 1);
        let l = unsafe { &mut *loader };
        let ptr = unsafe { &mut *loader }.ptr.unwrap();
        let r = unsafe { &mut *ptr };
        dbg!(ptr);
        assert_eq!(unsafe { &mut *ptr }.abc(), 2);
        assert_eq!(l.abc(), 3);
        assert_eq!(r.abc(), 4);

    }

    #[test]
    fn store_ptr_by_box() {
        let mut map:HashMap<i32, *mut O> = HashMap::new();
        let a = Box::new(O(3));
        // Box::into_raw 返回*mut T，获取Box管理的内存，
        // 需要清理时，调用Box::from_raw将裸指针转换为Box，自动进行清理
        map.insert(1, Box::into_raw(a));
        add_b_by_box(&mut map);
        assert!(map.len() == 2);
        assert!(unsafe { &**map.get(&1).unwrap() }.v() == 3);
        assert!(unsafe { &**map.get(&2).unwrap() }.v() == 4);
    }

    fn add_b_by_box(map: &mut HashMap<i32, *mut O>) {
        let b = Box::new(O(4));
        map.insert(2, Box::into_raw(b));
        assert!(unsafe { &**map.get(&2).unwrap() }.v() == 4);
    }

    #[test]
    fn store_ptr() {
        let mut map:HashMap<i32, *mut O> = HashMap::new();
        let mut a = O(3);
        map.insert(1, &mut a as *mut O);
        add_b(&mut map);
        assert!(map.len() == 2);
        assert!(unsafe { &**map.get(&1).unwrap() }.v() == 3);
        assert!(unsafe { &**map.get(&2).unwrap() }.v() != 4);
    }

    fn add_b(map: &mut HashMap<i32, *mut O>) {
        let mut b = O(4);
        map.insert(2, &mut b as *mut O);
        assert!(unsafe { &**map.get(&2).unwrap() }.v() == 4);
    }


    #[test]
    fn array_object() {
       let mut arr = ArrayObject::Byte(vec![0,1,2,3]);
       println!("{}", arr);
       let v = arr.bytes();
       v.push(4);
       v[0] = 100;
       println!("{}", arr);
    }

    #[test]
    fn copy_test() {
        let mut a = vec![1,2,3,4,1,2,3,4];
        let mut b = vec![5,6,7,8];
        let aa = &mut a[0..4];
        let bb = &mut b[0..4];
        aa.copy_from_slice(bb);
        assert_eq!(a, vec![5,6,7,8,1,2,3,4]);
        assert_eq!(b, vec![5,6,7,8]);
    }



    fn file_exists() {
        let path = Path::new("/home/lixun/Code/jvm-rust/java");
		let metadata = metadata(path);
		match metadata {
			Ok(_) => println!("file exists"),
			Err(e) => println!("not exists: {}", e)
		}
		// if metadata.is_file() {
		// 	dbg!("is file");
		// }
		// if metadata.is_dir() {
		// 	dbg!("is dir");
		// }
	}

    #[test]
    fn io_write() {
        let mut stdout = io::stdout();  
        let mut buffer: Vec<u8> = Vec::new();  
    
        // 将数据写入缓冲区  
        buffer.push(b'H');  
        buffer.push(b'i');  
    
        // 将缓冲区的数据写入标准输出  
        match stdout.write_all(&buffer) {  
            Ok(..) => println!("Data written to stdout"),  
            Err(err) => println!("Error writing to stdout: {}", err),  
        }  
    }


    #[test]
    fn entry_zip() {
        // // let zip = ZipEntry::new("java/test.zip");
        // let zip = ZipEntry::new("java/jre/rt.jar");
        
        // // let d = zip.read_class("HelloWorld.class");

        // let d = zip.read_class("java/lang/Object.class");
        // dbg!(d);
    }




// access_flags: u16,
// name: String, // 类名称
// super_class_name: String,
// interface_names: Vec<String>,
// constant_poll: Option<Rc<RefCell<ConstantPool>>>,
// fields: Vec<Arc<RefCell<Field>>>,
// methods: Vec<Arc<Mutex<Method>>>,
// loader: Option<*mut ClassLoader>,
// super_class: Option<Rc<RefCell<Class>>>,
// interfaces: Vec<Rc<RefCell<Class>>>,
// instance_slot_count: usize,
// static_slot_count: usize,
// static_vars: Slots
// }
// impl fmt::Display for Class {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         write!(f, "Class:{{ access_flags: {}, name: {}, super_class_name: {}, constant_poll: {}, fields: len {}, method: len {}, loader: {}, super_class: {}, interfaces: len {}, instance_slot_count: {}, static_slot_count: {}}}",
//             self.access_flags,
//             self.name,
//             self.super_class_name,
//             if self.constant_poll.is_none() {"None"} else {"Some"},
//             self.fields.len(),
//             self.methods.len(),
//             if self.loader.is_none() {"None"} else {"Some"},
//             if self.super_class.is_none() {"None"} else {"Some"},
//             self.interfaces.len(),
//             self.instance_slot_count,
//             self.static_slot_count,
//         )
//     }
// }


    #[test]
    fn class() {
        let mut class = Class::default();

        // RefCell
        assert!(class.init_started() == false);
        // class.strat_init();
        assert!(class.init_started() == true);

        let a = &mut class as *mut Class;
        let b = &class as *const Class;
        dbg!(a);
        dbg!(b);
        {
            let ac = unsafe { &*a };
            let bc = unsafe { &*b };
            assert!(std::ptr::eq(ac, bc));
        }
        let arc = Arc::new(class);
        let ac = unsafe { &*a };
        let aref = arc.as_ref();
        assert!(!std::ptr::eq(ac, aref));
        let c = aref as *const Class;
        assert!(b != c);
        dbg!(c);
    }



    #[test]
    fn entry_dir() {
        let dir = DirEntry::new("java/jar");
        // dbg!(dir);
        // let class = dir.read_class("rt.jar");
        // dbg!(class);
    }

    #[test]
    fn class_reader() {
        let data = vec![0,1,2,3,4,5,6,7,8,9];
        let mut class_reader = ClassReader::new(data);

        fn t(reader: &mut ClassReader) {
            dbg!(reader.read_u8());
        }
        // dbg!(class_reader.read_bytes(1));
        t(&mut class_reader);
        dbg!(class_reader.read_u8());
        dbg!(class_reader.read_u8());
        dbg!(class_reader.read_bytes(0));
        dbg!(class_reader.read_u8());
        // dbg!(class_reader.read_bytes(3));
        // dbg!(class_reader.read_bytes(4));
    }

    #[test]
    fn registry() {
        type F = fn(i32) -> i32;
        let f: F = |x| x + 1;
        assert_eq!(f(3), 4);
        struct A {
            pub x: F,
            y: F
        }
        let a = A{x: f, y: |x| x * 2};
        let x = a.x;
        let y = a.y;
        assert_eq!(x(2), 3);
        assert_eq!(y(2), 4);
        let v = vec![x, y];
        assert_eq!(v[0](2), 3);
        assert_eq!(v[1](2), 4);

        let mut map: HashMap<i32, F> = HashMap::new();
        map.insert(1, |x| x + 1);
        map.insert(-1, |x| x - 1);
        assert_eq!(map.get(&2), None);

        type NF = fn(i32);
        let a: NF = |x| println!("{}", x);
        let b: NF = |_| ();
        b(1);
        a(3);

    }

    #[test]
    fn test_lib() {
        // jvm_rust::test_lib()
        // let vv = raw_pointer!("s", "a");
        // dbg!(vv);
    }

    #[test]
    fn get_time_millis(){
        let now = SystemTime::now();
        let epoch = UNIX_EPOCH;

        // Convert SystemTime to Duration and then to milliseconds
        let duration = now.duration_since(epoch).unwrap();
        let milliseconds = duration.as_millis() as i64;
        dbg!(milliseconds);
    }

    #[test]
    fn os_test() {
        // let os_name = env::var("os").unwrap();
        // println!("OS: {}", os_name);
        let vars = env::vars();
        dbg!(vars);
    }

    #[test]
    fn clone_sturct_by_ptr() {
        #[derive(Clone, Debug)]
        struct A {val: i32}

        let a = Box::into_raw(Box::new(A{val: 1}));
        let b = Box::into_raw(Box::new(unsafe { &mut *a }.clone()));
        let c = b.clone();
        unsafe {
            { &mut *b }.val = 10;
            dbg!(&*a);
            dbg!(&*b);
            dbg!(&*c);
        }

    }

    #[test]
    fn raw_pointer() {
        // 测试指针与usize相互转换
        let mut val = 1008611usize;
        let ptr = &mut val as *mut usize;
        // let ptrval = unsafe { std::mem::size_of_val(&ptr) };
        dbg!(ptr);
        dbg!(ptr as i32);
        let ptrval = ptr as usize;
        dbg!(ptrval);
        println!("{:#x}", ptrval);
        let ptr2: *mut usize = unsafe { std::mem::transmute(ptrval) };
        dbg!(unsafe { *ptr2 });

    }

    fn test_4() {
        dbg!(bool::default());
        #[derive(Debug, Clone)]
        pub struct Q {pub v: RefCell<i32>}

        #[derive(Debug, Clone)]
        pub struct P {pub v: *mut i32}
        #[derive(Debug, Clone)]
        pub struct F<'a> {
            pub v: i32,
            pub q: &'a Q,
            pub p: &'a P,
        }
        let mut num = 42;
        let ptr = &mut num as *mut _;
        let mut q = Q{v: RefCell::new(3)};
        let mut p = P{v: ptr};
        let val = unsafe {
            let v = p.v;
            *v
        };
        dbg!(val);
        let mut f = F{v: val, q: &q, p: &p };
        dbg!(f.clone());
        *q.v.borrow_mut() = 4;
        let val = unsafe {
            let v = p.v;
            *v = 10000;
            *v
        };
        dbg!(val);
        dbg!(f.clone());
        let mut num = 42;
        let ptr = &mut num as *mut _;

        // 获取值
        let value = unsafe { *ptr };
        println!("Value: {}", value); // 输出：Value: 42

        // 修改值
        unsafe {
            *ptr = 100;
        }
        println!("Modified value: {}", num); // 输出：Modified value: 100

    }

    fn test_3(){
        let int = Int{val: 666};
        let a: Rc<RefCell<Box<dyn Any>>> = Rc::new(RefCell::new(Box::new(int)));
        let b: Rc<RefCell<Box<dyn Any>>> = Rc::new(RefCell::new(Box::new(Int{val:111})));
        let aa = a.as_ref() as *const _;
        let bb = b.as_ref() as *const _;
        dbg!(aa == bb);
        dbg!(0xfff3u16 as i16);
        dbg!(0xfff3u16);

    }

    fn test_2(){
    //    let u = usize::MAX;
    //    let ub = u.to_be_bytes();
    //    dbg!(ub);
    //    let i = -5.0f64 as usize;
    //    let ib = i.to_be_bytes();
    //    dbg!(ib);

       let f1 = 24.20;
       let f2 = 6.00;
       dbg!(f1 % f2);
       dbg!(i64::MIN as usize >> 1);
       dbg!("f1 as i32");
       dbg!(f1 as i32);
       dbg!(f1 as i64);
       dbg!(f1 as f32);

       dbg!(-5 & 0xffff);
       dbg!(-5i32 as i16);
    }

    fn test_1() {
        let u:[u8; 8] = [128,0,0,222,0,0,0,255];
        let i = i64::from_be_bytes(u);
        let i1 = (i >> 32) as i32;
        let i2 = i as i32;
        dbg!(i1.to_be_bytes());
        dbg!(i2.to_be_bytes());
        let o1 = i1 as i64;
        dbg!(o1.to_be_bytes());
        let o2 = i2 as i64;
        dbg!(o2.to_be_bytes());
        let o = (o1 << 32) + o2;
        dbg!(o.to_be_bytes());

        let rc: Rc<RefCell<Box<dyn Any>>> = Rc::new(RefCell::new(Box::new(Int{val: 1})));
        let da:Option<Rc<RefCell<Box<dyn Any>>>> = Some(Rc::new(RefCell::new(Box::new(Int{val: 1}))));
        let aa = da.unwrap().clone();
        let bb = aa.borrow();
        let int = bb.downcast_ref::<Int>().unwrap();
        // let int = da.unwrap().borrow().downcast_ref::<Int>().unwrap();
        dbg!(int.val);

        // let s = -1i32 as u32;
        // dbg!(s >> 1);
        // dbg!(s << 1);
        // dbg!(-1i32 as i64);
        // let z = u32::MAX as i64;
        // dbg!(z);
        // let s = (1i64 << 63) + (1i64 << 31);
        // dbg!(s.to_be_bytes());
        // dbg!((s >> 33) as i32);
        // dbg!(s as i32);
        // let u:[u8; 8] = [0,255,255,255,255,255,255,255];
        // let i = i64::from_be_bytes(u);
        // dbg!(i);
        // // dbg!((i as i32).to_be_bytes());
        // dbg!((-1i32 as i64) << 32);
        // dbg!((i << 32).to_be_bytes());
        // dbg!((-1i32 as i64).to_be_bytes());
        // dbg!(-1i64 << 32);
        // dbg!((-1i64 << 32).to_be_bytes());
    }

    fn test_f32() {
        let bt = 5.0f32.to_be_bytes();
        dbg!(bt.clone());
        let i32 = i32::from_be_bytes(bt);
        dbg!(i32);
        let f32 = 0b01000000101000000000000000000000;
        dbg!(f32);
        // let f32 = f32::from_be_bytes(f.to_be_bytes());

        let mut x = 0i64;
        x += (1 << 10);
        x += (1 << 40);
        dbg!(x as i32);
        dbg!((x >> 32) as i32);
    }

    fn test_dyn_any() {


        // let x = 112;
        // let r = &x as *const i32;
        // dbg!(r);
        pub struct Int {
            pub val: i32
        }

        impl Int {
            pub fn as_any(&self) -> &dyn Any {
                self
            }

            // pub fn any(&mut self) -> dyn Any {
            //     self
            // }
        }

        let x = 1;
        let a: &dyn Any = &x;

        // let int = Int{val: 11};

        // let any= int.as_any();
        // let x = 1;
        // let a: &dyn Any = &x;

        // let mut any:Option<&dyn Any> = None;

        // {
        //     let x = Int{val: 11};
        //     any = Some(&x)
        // }

        // let int = any.unwrap().downcast_ref::<Int>();

        let mut v = Vec::with_capacity(0);
        dbg!(v.capacity());
        v.push(1);
        dbg!(v.capacity());
        v.push(1);
        v.push(1);
        dbg!(v.capacity());
    }

    fn test_vec_time() {
         // let now = SystemTime::now();
        // // 我们睡了 2 秒钟
        // sleep(Duration::new(2, 0));
        // match now.elapsed() {
        //     Ok(elapsed) => {
        //         // 它打印 '2'
        //         println!("{}", elapsed.as_millis());
        //     }
        //     Err(e) => {
        //         // 发生错误！
        //         println!("Error: {e:?}");
        //     }
        // }

        // time_vec();
        // time_array();
        // time_vec();
        // time_array();
        // vec_drain();
    }

    fn vec_drain() {
        let mut vec: Vec<u8> = vec![0; 1_000_000];
        let now = SystemTime::now();

        for _ in 0..100_000 {
            vec.drain(..10);
        }

        match now.elapsed() {
            Ok(elapsed) => {
                println!("time: {}", elapsed.as_millis());
            }
            Err(e) => {
                println!("Error: {e:?}");
            }
        }
        dbg!(vec.len());

    }

    fn time_vec() {
        let mut vec: Vec<u8> = vec![0; 10_00_000];
        let mut sum: u8 = 0;
        let now = SystemTime::now();

        for i in 0..10_00_000 {
            sum += vec[i];
        }

        match now.elapsed() {
            Ok(elapsed) => {
                // 它打印 '2'
                println!("time: {}", elapsed.as_millis());
            }
            Err(e) => {
                // 发生错误！
                println!("Error: {e:?}");
            }
        }

        dbg!(sum);
    }

    fn time_array() {
        let arr: [u8; 10_00_000] = [0; 10_00_000];
        let mut sum: u8 = 0;
        let now = SystemTime::now();

        for i in 0..10_00_000 {
            sum += arr[i];
        }

        match now.elapsed() {
            Ok(elapsed) => {
                // 它打印 '2'
                println!("time: {}", elapsed.as_millis());
            }
            Err(e) => {
                // 发生错误！
                println!("Error: {e:?}");
            }
        }

        dbg!(sum);
    }

    fn str_equals() {
        dbg!("----------------+++++++++++++++++++++++");
        let s1 = "abc".to_string();
        let s2 = "def".to_string();
        let s3 = "abc".to_string();
        dbg!(s1 == s2);
        dbg!(s1 == s3);
        dbg!(s1[..] == s3[..]);
    }

    fn args_and_path() {
        for (key, val) in env::vars() {
            println!("{} => {}", key, val);
        }
        let s = env::var("PATH");
        dbg!(s);
        dbg!(Path::new("/Library/Java/JavaVirtualMachines/jdk1.8.0_361.jdk/Contents/Home/jre").exists());
        dbg!(Path::new("java").exists());
        dbg!(Path::new("java/Test1.class").exists());
        dbg!(Path::new("java/Test11.class").exists());
        dbg!(0xCAFEBABEu32);
        dbg!(0xCAu32);
        dbg!(0xFEu32);
        dbg!(0xBAu32);
        dbg!(0xBEu32);
    }

    fn rc_refcell() {
        let mut a = Rc::new(RefCell::new(A{vec: vec![]}));
        let b1 = B1{name: "b1".into() };
        a.borrow_mut().add(Box::new(b1));
        let b2 = B2{a: a.clone(),name: "b2".into() };
        a.borrow_mut().add(Box::new(b2));
        let b3 = B3{a: a.clone(),name: "b3".into() };
        a.borrow_mut().add(Box::new(b3));
        let x = &a.borrow().vec;
        dbg!(x.len());
        let name = x[1].get(2);
        dbg!(name);
        let i = i32::default();
        dbg!(i);
    }
}
