#[cfg(test)]
mod java_tests {
    use super::super::run;

    #[test]
    // #[ignore]
    fn thread_test() {
        run("thread/ThreadTest");
    }

    #[test]
    // #[ignore]
    fn synchronized_test() {
        run("thread/ThreadSynchronized");
    }
    
    #[test]
    // #[ignore]
    fn daemon_test() {
        run("thread/ThreadDaemon");
    }
    
}