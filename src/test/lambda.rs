#[cfg(test)]
mod java_tests {
    use super::super::run;

    #[test]
    // #[ignore]
    fn method_type() {
        run("lambda/MethodTypeTest");
    }

    #[test]
    // #[ignore]
    fn lookup() {
        run("lambda/LookupTest");
    }

    #[test]
    // #[ignore]
    fn runnable() {
        run("lambda/LambdaRunnable");
    }

    #[test]
    // #[ignore]
    fn interface() {
        run("lambda/LambdaInterface");
    }

    #[test]
    // #[ignore]
    fn comparator() {
        run("lambda/LambdaComparator");
    }
    
}