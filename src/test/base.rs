#[cfg(test)]
mod java_tests {
    use super::super::run;


    // cargo run -- -Xjre /home/lixun/jdk1.8.0_381/jre -cp /home/lixun/Code/jvm-rust/java base/HelloWorld
    #[test]
    // #[ignore]
    fn helloworld() {
        run("base/HelloWorld");
    }

    #[test]
    // #[ignore]
    fn array_class_test() {
        run("base/ArrayClassTest");
    }

    #[test]
    // #[ignore]
    fn array_list_test() {
        run("base/ArrayListTest");
    }

    #[test]
    // #[ignore]
    fn box_test() {
        run("base/BoxTest");
    }

    #[test]
    // #[ignore]
    fn clone_test() {
        run("base/CloneTest");
    }


    #[test]
    // #[ignore]
    fn get_class_test() {
        run("base/GetClassTest");
    }

    #[test]
    // #[ignore]
    fn hash_map_test() {
        run("base/HashMapTest");
    }

    #[test]
    // #[ignore]
    fn object_test() {
        run("base/ObjectTest");
    }

    #[test]
    // #[ignore]
    fn println_test() {
        run("base/PrintlnTest");
    }

    #[test]
    // #[ignore]
    fn string_build_test() {
        run("base/StringBuilderTest");
    }

    #[test]
    // #[ignore]
    fn string_test() {
        run("base/StringTest");
    }

    #[test]
    // #[ignore]
    fn priority_queue_test() {
        run("base/PriorityQueueTest");
    }
    
    
}