
use crate::cmd::Cmd;

// 用于测试class文件 查看输出结果
mod thread;
mod lambda;
mod base;
mod code;



#[cfg(target_os = "windows")]
static X_JRE_OPTHIN: &'static str = "";

// JRE路径
#[cfg(target_os = "linux")]
static X_JRE_OPTHIN: &'static str = "/home/lixun/jdk1.8.0_381/jre";

#[cfg(target_os = "macos")]
static X_JRE_OPTHIN: &'static str = "/Library/Java/JavaVirtualMachines/jdk1.8.0_361.jdk/Contents/Home/jre";


#[cfg(target_os = "windows")]
static CP_OPTION: &'static str = "";

// 测试文件路径
#[cfg(target_os = "linux")]
static CP_OPTION: &'static str = "./java";

#[cfg(target_os = "macos")]
static CP_OPTION: &'static str = "/Users/lixun/Code/jvm-rust/java";

#[allow(warnings)]
fn run(name: &str) {
    let mut cmd = Cmd::new();
    cmd.x_jre_opthin = X_JRE_OPTHIN.to_string();
    // cmd.verbose_inst_falg = true;
    cmd.cp_option = CP_OPTION.to_string();
    cmd.class = name.to_string();

    crate::jvm_run(cmd);
}
