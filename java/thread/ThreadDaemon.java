package thread;

/**
 * @author lixun
 * @version 1.0
 * @description:
 * @date 2023/12/18 16:33
 */
public class ThreadDaemon {

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getPriority());
        // 创建一个守护线程
        Thread daemonThread = new Thread(() -> {
            System.out.println("守护优先级" + Thread.currentThread().getPriority());
            Thread a = new Thread(() -> {
                System.out.println("内部优先级" + Thread.currentThread().getPriority());
//                Thread.currentThread().countStackFrames();
                for (int i = 1; i <= 6; i++) {
                    try {
                        System.out.println("内部" + i);
                        Thread.sleep(1000L);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            a.setDaemon(false);
            a.start();
            for (int i = 1; i <= 10; i++) {
                try {
                    System.out.println("守护-" + i);
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        daemonThread.setDaemon(true); // 将守护线程设置为守护状态
        daemonThread.start();

        Thread a = new Thread(() -> {
            System.out.println("外部优先级" + Thread.currentThread().getPriority());

            for (int i = 1; i <= 3; i++) {
                try {
                    System.out.println("外部--" + i);
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        a.start();
    }
}
