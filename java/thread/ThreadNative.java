package thread;

/**
 * @author lixun
 * @date 2023/12/18-20:38
 * @since [v1.0]
 */
public class ThreadNative {

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> run('a'));
        Thread t2 = new Thread(() -> run('b'));
        Thread t3 = new Thread(() -> run('c'));
        t1.start();
        t2.start();
        t3.start();
    }

    private static void run(char name) {
        for (int i = 0; i < 11; i++) {
            System.out.println("name: " + name + " run" + i);
            if (i == 5) {
                Thread.yield();
            }
        }
    }
}
