package thread;

/**
 * @author lixun
 * @version 1.0
 * @description:
 * @date 2023/10/30 14:32
 */
public class ThreadSynchronized {
    static final Object lock = new Object();
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> run('a'));
        Thread t2 = new Thread(() -> run('b'));
        Thread t3 = new Thread(() -> run('c'));
        Thread t4 = new Thread(() -> run('d'));
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }

    private static void run(char name) {
        int i = 5;
        while (--i > 0) {
            synchronized (lock) {
                System.out.println("thread: " + name + " run");
            }
        }
    }
}
