package thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class ThreadTest {
    public static void main(String[] args) {
        // 继承 Thread 类
        MyThread myThread = new MyThread();
        myThread.start();

        // 实现 Runnable 接口
        Thread runnable = new Thread(() -> System.out.println("Runnable run"));
        runnable.start();

        //  Callable 和 Future
        FutureTask<Integer> ft = new FutureTask<>(() -> {
            System.out.println("Callable run");
            return ThreadTest.fibonacci(26);
        });
        Thread callable = new Thread(ft);
        callable.start();
        try
        {
            System.out.println("Callable fibonacci(26) return: "+ft.get());
        } catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }
    }

    private static int fibonacci(int n) {
        if (n <= 1) {
            return n;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }
}

class MyThread extends Thread {
    public void run() {
        System.out.println("MyThread run");
    }
}
