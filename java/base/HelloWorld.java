package base;

/**
 * @author lixun
 * @version 1.0
 * @description:
 * @date 2023/12/20 12:48
 */
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

}
