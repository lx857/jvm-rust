package base;

import java.util.PriorityQueue;

/**
 * @author lixun
 * @version 1.0
 * @description:
 * @date 2023/12/19 17:09
 */
public class PriorityQueueTest {

    public static void main(String[] args) {
        PriorityQueue<Integer> queue = new PriorityQueue<>(((o1, o2) -> o2 - o1));
        queue.offer(1);
        queue.offer(12);
        queue.offer(123);
        queue.offer(13);
        queue.offer(1234);
        queue.offer(14);
        while (!queue.isEmpty()) {
            System.out.println(queue.poll());
        }
    }
}
