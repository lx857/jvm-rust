import java.lang.invoke.MethodType;

/**
 * @author lixun
 * @date 2023/11/21-19:47
 * @since [v1.0]
 */
public class MethodTypeTest {
    public static void main(String[] args) {
        MethodType methodType = MethodType
                .fromMethodDescriptorString(
                        "(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;", null);
        System.out.println(methodType);
    }
}
