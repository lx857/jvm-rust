import java.util.Arrays;

public class ClassDemo1 {

   public static class C3{
      public C3() {
      }
   }

   public ClassDemo1() {
   }

   public static void main(String[] args) {
      C3 c3 = new C3();
      System.out.println(c3.getClass().getSimpleName());
      C3[] c3a = new C3[3];
      System.out.println(c3a.getClass().getSimpleName());
      ClassDemo1 c1 = new ClassDemo1();
      System.out.println(c1.getClass().getSimpleName());
   }
}
