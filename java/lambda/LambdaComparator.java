package lambda;

import java.util.Arrays;

/**
 * @author lixun
 * @version 1.0
 * @description:
 * @date 2023/12/14 15:46
 */
public class LambdaComparator {

    public static void main(String[] args) {
        int[][] arr = new int[][]{{2,3},{2,6},{2,1},{3,5},{1,3},{6,6},{1,1}};
        Arrays.sort(arr, (a, b) -> {
            if (a[0] == b[0]) {
                return a[1] - b[1];
            }
            return a[0] - b[0];
        });
        for (int[] ints : arr) {
            System.out.println("{" + ints[0] + "," + ints[1] + "}");
        }
    }
}
