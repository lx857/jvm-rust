package lambda;

import java.lang.invoke.*;

/**
 * @author lixun
 * @version 1.0
 * @description:
 * @date 2023/11/22 16:18
 */
public class LookupTest {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException {
//        MethodHandles.Lookup lookup = MethodHandles.lookup();
//        MethodType methodType = MethodType.fromMethodDescriptorString("(Ljava/lang/Object;)Ljava/lang/String;", null);
//        MethodHandle valueOf = lookup.findStatic(String.class, "valueOf", methodType);
//        System.out.println(valueOf);
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodType methodType = MethodType.fromMethodDescriptorString("(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodHandle;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;", null);
        MethodHandle valueOf = lookup.findStatic(LambdaMetafactory.class, "metafactory", methodType);
        System.out.println(valueOf);
//        MethodHandles.Lookup lookup = MethodHandles.lookup();
//        MethodHandle valueOf = lookup.findStatic(CallSite.class, "uninitializedCallSite", MethodType.methodType(Object.class, Object[].class));
//        System.out.println(valueOf);
    }
}
