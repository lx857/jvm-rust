package lambda;

/**
 * @author lixun
 * @version 1.0
 * @description:
 * @date 2023/12/14 14:28
 */
public class LambdaInterface {

    public static void main(String[] args) {
        LambdaInterfaceRun r = (i) -> {
            switch (i) {
                case 10000:
                case 10001:
                    System.out.print("电信：");
                    break;
                case 10010:
                    System.out.print("联通：");
                    break;
                case 10086:
                    System.out.print("移动：");
                    break;
                case 10099:
                    System.out.print("广电：");
                    break;
                default :
                    System.out.print("未知号码：");
            }
            System.out.println(i);
        };
        r.run(10000);
        r.run(10086);
        r.run(10001);
        r.run(10099);
        r.run(10010);
        r.run(11111);

//        LambdaInterfaceRun r1 = System.out::println;
//        r1.run(10000);
    }
}

interface LambdaInterfaceRun {
    void run(int a);
}
