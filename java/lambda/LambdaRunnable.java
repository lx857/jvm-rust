package lambda;

/**
 * @author lixun
 * @date 2023/11/18-20:14
 * @since [v1.0]
 */
public class LambdaRunnable {

    public static void main(String[] args) {
        Runnable fn = () -> System.out.println(10086);
        fn.run();
        Runnable fn1 = () -> run("call function");
        fn1.run();
    }

    private static void run(String name) {
        System.out.println(name + " run");
    }
}
